import React, { PropTypes, Component } from "react"
import { connect } from "react-redux"
import { Link } from "react-router"
import verifyEmail from "../../actions/auth/verifyEmail"

class Verification extends Component {
    componentDidMount() {
        this.props.dispatch(verifyEmail({
            code: this.props.location.query.code
        }))
    }

    render() {
        let { message } = this.props

        return (
            <div className="text-center">
                <div className="row">
                    <div className="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3">
                        <div className="">
                            <h1 className="">{"正在验证, 请等待"}</h1>
                            <p className="lead text-muted">...</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

Verification.propTypes = {
    message: PropTypes.shape({
        message: PropTypes.string.isRequired
    })
}

export default connect(
    state =>({
        message: state.message
    })
)(Verification)
