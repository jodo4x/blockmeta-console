import { get } from "../../middleware/api"
import {
    QUERY_ASSETS_REQUEST,
    QUERY_ASSETS_SUCCESS,
    QUERY_ASSETS_FAILURE
} from "../../constants/actionTypes"

export default (data) =>
    dispatch => {
        dispatch(queryAssetsRequest)

        const param = (data.page || data.page === 0) ? `?page=${data.page}` : `?q=${data.q}`

        get(`/assets${param}`, data)
        .then(json => {
            dispatch(queryAssetsSuccess(json.data))
        }, err => {
            dispatch(queryAssetsFailure({
                status: "warning",
                message: "哎呀，出现了一些错误，请再试一次。"
            }))
        })
    }

const queryAssetsRequest = {
    type: QUERY_ASSETS_REQUEST
}

function queryAssetsSuccess(data) {
    return {
        type: QUERY_ASSETS_SUCCESS,
        payload: {
            assets: data
        }
    }
}

function queryAssetsFailure(message) {
    return {
        type: QUERY_ASSETS_FAILURE,
        payload: {
            message
        }
    }
}
