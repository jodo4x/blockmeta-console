import {
    SIGNUP_SUCCESS,
    SIGNUP_FAILURE
} from "../../constants/actionTypes"
import { assign } from "../../utils"

export default (state, { type, payload }) => {
    switch(type) {
        case SIGNUP_SUCCESS:
            return assign({}, state, {
                isSignedUp: true
            })
        case SIGNUP_FAILURE:
            return assign({}, state, {
                message: payload.message
            })
        default:
            return state
    }
}
