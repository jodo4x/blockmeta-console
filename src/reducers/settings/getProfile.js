import {
    GET_PROFILE_SUCCESS,
    GET_PROFILE_FAILURE
} from "../../constants/actionTypes"
import { assign } from "../../utils"

export default (state, { type, payload }) => {
    switch(type) {
        case GET_PROFILE_SUCCESS:
            return assign({}, state, {
                profile: payload.data
            })
        case GET_PROFILE_FAILURE:
            return assign({}, state, {
                message: payload.message
            })
        default:
            return state
    }
}
