import React, { PropTypes, Component } from "react"
import { Link } from "react-router"
import { Dropdown } from "react-bootstrap"

export default class Menubar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isDropdownOpen: false
        }
    }

    render() {
        let { notifications, profile, handleLogout, handleToggleNavbar, isNavbarCollapsed } = this.props

        return (
            <div className="browser-menubar clearfix">
                <ul className="nav pull-left">
                    <li>
                        <a
                            href="javascript:;"
                            onClick={handleToggleNavbar}
                        >
                            <i style={{ fontSize: 16, marginTop: 3 }} className={"fa fa-" + (isNavbarCollapsed ? "indent" : "outdent")}></i>
                        </a>
                    </li>
                </ul>

                <ul className="nav nav-pills pull-right">
                    {notifications &&
                    <li>
                        <Link to="/notifications">
                            <i className="fa fa-bell-o" style={{ fontSize: 18 }}></i>
                            {!!notifications.unread &&
                                <sup className="badge badge-danger">
                                    {notifications.unread}
                                </sup>}
                        </Link>
                    </li>}
                    {profile &&
                    <li className={"dropdown" + (this.state.isDropdownOpen ? " open" : "")} style={{ paddingRight: 15 }}>
                        <Dropdown id="dropdown001">
                            <Dropdown.Toggle>
                                <img src={"//" + profile.avatar} alt="" style={{ width: 22, height: 22 }} />
                                <span>{profile.nickname || profile.email}</span>
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                    <li>
                                        <Link to="/settings/profile">设置</Link>
                                    </li>
                                    <li className="divider"></li>
                                    <li>
                                        <a href="javascript:;" onClick={handleLogout}>登出</a>
                                    </li>
                            </Dropdown.Menu>
                        </Dropdown>
                    </li>}
                </ul>
            </div>
        )
    }
}
