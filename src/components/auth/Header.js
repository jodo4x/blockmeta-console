import React from "react"
import { Link } from "react-router"

const Header = () => (
    <div className="page-header text-center" style={{ borderBottom: 0 }}>
        <h1><Link to="/login">BlockMeta</Link></h1>
    </div>
)

export default Header
