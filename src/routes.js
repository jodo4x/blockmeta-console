import { cookie } from "./utils"
import React from "react"
import { Route, IndexRedirect } from "react-router"
import App from "./containers/App"
import Auth from "./containers/auth"
import Login from "./containers/auth/Login"
import GetRecoveryPasswordEmail from "./containers/auth/GetRecoveryPasswordEmail"
import ResetPassword from "./containers/auth/ResetPassword"
import Signup from "./containers/auth/Signup"
import Verification from "./containers/auth/Verification"
import Browser from "./containers/Browser"
import Settings from "./containers/settings"
import Profile from "./containers/settings/Profile"
import Password from "./containers/settings/Password"
import Notifications from "./containers/notifications"
import Assets from "./containers/assets"
import Mining from "./containers/mining"

function requireAuth(nextState, replace) {
    if (!cookie.get("_at_")) {
        replace({
            pathname: "/login",
            state: { nextPathname: nextState.location.pathname }
        })
    }
}

export default (
    <Route component={App}>

        <Route component={Auth}>
            <Route path="/login" component={Login} />
            <Route path="/recovery-password" component={GetRecoveryPasswordEmail} />
            <Route path="/reset-password" component={ResetPassword} />
            <Route path="/signup" component={Signup} />
            <Route path="/verification" component={Verification} />
        </Route>

        <Route path="/" component={Browser} onEnter={requireAuth}>

            <IndexRedirect to="/assets" />

            <Route component={Settings}>
                <Route path="/settings/profile" component={Profile} />
                <Route path="/settings/password" component={Password} />
            </Route>
            <Route path="/notifications" component={Notifications} />

            <Route path="/assets" component={Assets} />

            <Route path="/mining" component={Mining} />
        </Route>

    </Route>
)
