import React from "react"
import { findDOMNode } from "react-dom"
import addressTypes from "../../constants/addressTypes"
import Select from "react-select"

export default class NewAssetRow extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            typeValue: "Business" // 默认的值
        }
        this.getTypeValue = this.getTypeValue.bind(this)
    }

    getTypeValue() {
        return this.state.typeValue
    }

    render() {
        return (
            <tr className="nar" id="J__tr">
                <td>
                    <input
                        className="form-control"
                        type="text"
                        ref={this.props.addr}
                        placeholder="输入地址"
                    />
                </td>
                <td>
                    <Select
                        options={addressTypes.map((item, index) => {
                            return {
                                value: item.key,
                                label: item.name
                            }
                        })}
                        clearable={false}
                        searchable={false}
                        value={this.state.typeValue}
                        onChange={(selected) => {
                            this.setState({
                                typeValue: selected.value
                            })
                        }}
                    />
                </td>
                <td>
                    <input
                        className="form-control"
                        type="text"
                        ref={this.props.mark}
                        placeholder="添加备注"
                    />
                </td>
                <td className="text-right">
                    <button className="btn btn-link" onClick={() => {
                        this.props.handleHideNewAsset()
                    }}>取消</button>
                    <button
                        className="btn btn-link"
                        onClick={this.props.handleCreateAsset}
                    >
                        <span>保存</span>
                    </button>
                </td>
            </tr>
        )
    }
}
