import { get } from "../../middleware/api"

export default function getMarket() {
    return dispatch => {
        get("/meta/markets")
        .then(json => {
            dispatch(getMarketSuccess(json.data))
        }, err => {
            // TODO
        })
    }
}

function getMarketSuccess(market) {
    return {
        type: "GET_MARKET_SUCCESS",
        payload: {
            market
        }
    }
}
