import { get } from "../../middleware/api"
import {
    GET_PROFILE_REQUEST,
    GET_PROFILE_SUCCESS,
    GET_PROFILE_FAILURE
} from "../../constants/actionTypes"

export default () => {
    return dispatch => {
        dispatch(getProfileRequest)

        get("/users/me")
        .then(json =>
            dispatch(getProfileSuccess(json))
        , error =>
            dispatch(getProfileFailure({
                status: "warning",
                message: "哎呀，出现了一些错误，请刷新页面试试"
            }))
        )
    }
}

const getProfileRequest = {
    type: GET_PROFILE_REQUEST
}

function getProfileSuccess(data) {
    return {
        type: GET_PROFILE_SUCCESS,
        payload: {
            data
        }
    }
}

function getProfileFailure(message) {
    return {
        type: GET_PROFILE_FAILURE,
        payload: {
            message
        }
    }
}
