import "whatwg-fetch"
import { push } from "react-router-redux"
import { cookie } from "../utils"
import BASE_URL from "../constants/baseUrl"
import { assign } from "../utils"
import store from "../store"

function callApi(endpoint, request) {
    return fetch(
        /htt(p|ps):[\/]{2}/.test(endpoint) ? endpoint : `${BASE_URL + endpoint}`,
        request
    )
    .then(response => {
        return response.json()
        .then(json => ({ response, json }))
    })
    .then(({ response, json }) => {
        return response.ok ?
            Promise.resolve(json) : (function() {
                if (response.status === 401) {
                    cookie.remove("_at_")
                    store.dispatch(push("/login"))
                }
                return Promise.reject(json)
            }())
    })
}

export function get(endpoint, params, options = null) {
    return callApi(endpoint, assign({
        method: "GET",
        headers: {
            Authorization: `Basic ${btoa(":" + cookie.get("_at_"))}` // normal `GET`
        }
    }, options/* login `GET` will override the `headers` */))
}

export function post(endpoint, params) {
    return callApi(endpoint,
        Object.prototype.toString.call(params) === "[object FormData]" ?
        {
            method: "POST",
            body: params // `formData` already contains `policy & signature`
        } :
        {
            method: "POST",
            headers: {
                Authorization: `Basic ${btoa(":" + cookie.get("_at_"))}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify(params)
        }
    )
}

export function put(endpoint, params) {
    return callApi(endpoint, {
        method: "PUT",
        headers: {
            Authorization: `Basic ${btoa(":" + cookie.get("_at_"))}`,
            "Content-Type": "application/json"
        },
        body: JSON.stringify(params)
    })
}

export function patch(endpoint, params) {
    return callApi(endpoint, {
        method: "PATCH",
        headers: {
            Authorization: `Basic ${btoa(":" + cookie.get("_at_"))}`,
            "Content-Type": "application/json"
        },
        body: JSON.stringify(params)
    })
}

export function destroy(endpoint) {
    return callApi(endpoint, {
        method: "DELETE",
        headers: {
            Authorization: `Basic ${btoa(":" + cookie.get("_at_"))}`,
        }
    })
}
