import { get } from "../../middleware/api"
import {
    GET_MINING_INFO_REQUEST,
    GET_MINING_INFO_SUCCESS,
    GET_MINING_INFO_FAILURE
} from "../../constants/actionTypes"

export default function getMiningInfo(params) {
    return dispatch => {
        dispatch(getMiningInfoRequest)

        get(`/mining/mining-info?timespan=${params.timespan}`)
        .then(json => {
            dispatch(getMiningInfoSuccess(json))
        }, err => {
            dispatch(getMiningInfoFailure({
                status: "warning",
                message: "哎呀，出现了一些错误，请稍后再试。"
            }))
        })
    }
}

const getMiningInfoRequest = {
    type: "GET_MINING_INFO_REQUEST",
    payload: {
        isFetching: true
    }
}

function getMiningInfoSuccess(json) {
    return {
        type: GET_MINING_INFO_SUCCESS,
        payload: {
            miningInfo: json,
            isFetching: false
        }
    }
}

function getMiningInfoFailure(message) {
    return {
        type: GET_MINING_INFO_FAILURE,
        payload: {
            message,
            isFetching: false
        }
    }
}
