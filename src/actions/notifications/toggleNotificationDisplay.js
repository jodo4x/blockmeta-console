import { TOGGLE_NOTIFICATION_DISPLAY } from "../../constants/actionTypes"

export default (id) => (
    {
        type: TOGGLE_NOTIFICATION_DISPLAY,
        payload: {
            id
        }
    }
)
