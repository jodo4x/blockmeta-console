export default [
    { "key": "Business", "name": "商业" },
    { "key": "Personal", "name": "个人" },
    { "key": "Donation", "name": "捐助" },
    { "key": "Hack"    , "name": "黑客" },
    { "key": "Gambling", "name": "博彩" },
    { "key": "Other"   , "name": "其它" }
]
