import React from "react"

const Spinner = () =>
    <span className="spinner">
        {[1, 2, 3, 4, 5].map((num, index) => {
            return <span className={"rect" + num} key={index}></span>
        })}
    </span>

export default Spinner
