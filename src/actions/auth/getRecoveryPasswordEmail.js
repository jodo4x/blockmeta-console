import { post } from "../../middleware/api"
import {
    GET_RECOVERY_PASSWORD_EMAIL_REQUEST,
    GET_RECOVERY_PASSWORD_EMAIL_SUCCESS,
    GET_RECOVERY_PASSWORD_EMAIL_FAILURE
} from "../../constants/actionTypes"

export default (data) => {
    return dispatch => {
        dispatch(getRecoveryPasswordRequest)

        post(`/users/me/forget-password`, data)
        .then(json => {
            dispatch(getRecoveryPasswordSuccess({
                status: "info",
                message: `我们已经把重置密码的地址发送到你的邮箱${data.email}`
            }))
        }, error => {
            const messageMap = {
                "1013": `邮箱${data.email}还未注册`,
                "1004": "不是有效的邮箱，请重新再试一次"
            }

            dispatch(getRecoveryPasswordFailure({
                status: "warning",
                message: messageMap[error.code] || "哎呀，出现了一些其它错误"
            }))
        })
    }
}

const getRecoveryPasswordRequest = {
    type: GET_RECOVERY_PASSWORD_EMAIL_REQUEST
}

function getRecoveryPasswordSuccess(message) {
    return {
        type: GET_RECOVERY_PASSWORD_EMAIL_SUCCESS,
        payload: {
            message
        }
    }
}

function getRecoveryPasswordFailure(message) {
    return {
        type: GET_RECOVERY_PASSWORD_EMAIL_FAILURE,
        payload: {
            message
        }
    }
}
