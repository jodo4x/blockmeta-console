import {
    UPDATE_PROFILE_SUCCESS,
    UPDATE_PROFILE_FAILURE
} from "../../constants/actionTypes"
import { assign, assignOnlyTrueValue } from "../../utils"

export default (state, { type, payload }) => {
    switch(type) {
        case UPDATE_PROFILE_SUCCESS:
            return assign({}, state, {
                message: payload.message,
                profile: assignOnlyTrueValue({}, state.profile, payload.data)
            })
        case UPDATE_PROFILE_FAILURE:
            return assign({}, state, {
                message: payload.message
            })
        default:
            return state
    }
}
