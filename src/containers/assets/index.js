import React, { Component } from "react"
import { connect } from "react-redux"
import Overview from "../../components/assets/Overview"
import AssetsManager from "../../components/assets/AssetsManager"
import MonitorModal from "../../components/assets/MonitorModal"

import loadMyAssets from "../../actions/assets/loadMyAssets"
import clickAddAsset from "../../actions/assets/clickAddAsset"
import createAsset from "../../actions/assets/createAsset"
import deleteAsset from "../../actions/assets/deleteAsset"
import getModalAsset from "../../actions/assets/getModalAsset"
import addAssetMonitor from "../../actions/assets/addAssetMonitor"
import queryAssets from "../../actions/assets/queryAssets"
import getAsset from "../../actions/assets/getAsset"
import changeControlledComponent from "../../actions/changeControlledComponent"

export default connect(
    state => ({
        isMonitorUpdated: state.isMonitorUpdated||false,
        isAssetDeleted: state.isAssetDeleted,
        defaultNotifyEmail: state.profile && state.profile.email || "",
        statistics: state.statistics,
        isAddNewAsset: state.isAddNewAsset,
        assets: state.assets,
        modalAsset: state.modalAsset||{}
    })
)(class Assets extends Component {
    componentDidMount() {
        this.props.dispatch(loadMyAssets())

        /*
        document.body.addEventListener("click", (e) => {
            var tr = document.getElementById("J__tr")
            var thead = document.getElementById("J__thead")

            if (tr && thead) {
                if (tr.contains(e.target)) {
                    tr.classList.remove("hidden")
                } else if (thead.contains(e.target)) {
                    tr.classList.remove("hidden")
                } else {
                    this.props.dispatch(clickAddAsset(false))
                }
            }
        })
        */
    }

    render() {
        let modal, addr, type, mark
        let monitor, monitorModal, inMin, outMin, notifyEmail

        return (
            <div className="addr-container" style={{ paddingTop: 20, paddingBottom: 20 }}>
                <Overview
                    statistics={this.props.statistics}
                />

                <AssetsManager
                    addr={node => addr = node}
                    type={node => type = node}
                    mark={node => mark = node}
                    isAddNewAsset={this.props.isAddNewAsset}
                    isAssetDeleted={this.props.isAssetDeleted}
                    assets={this.props.assets||{items: []}}
                    handleHideNewAsset={() => this.props.dispatch(clickAddAsset(false))}
                    handleAddAsset={() => this.props.dispatch(clickAddAsset(true))}
                    handleCreateAsset={() => this.props.dispatch(createAsset({
                        address: addr.value.trim(),
                        type: type.getTypeValue(),
                        mark: mark.value.trim()
                    }))}
                    handleAddMonitor={(id) => {
                        this.props.dispatch(getModalAsset(id))
                        modal.open()
                    }}
                    handleDeleteAsset={(id) => {
                        this.props.dispatch(deleteAsset(id))
                    }}

                    handlePagerPrev={() => {
                        this.props.dispatch(
                            queryAssets({
                                page: this.props.assets.page - 1
                            })
                        )
                    }}
                    handlePagerNext={() => {
                        this.props.dispatch(
                            queryAssets({
                                page: this.props.assets.page + 1
                            })
                        )
                    }}
                    handlePager={(pager) => {
                        this.props.assets.page = pager
                        this.props.dispatch(
                            queryAssets({
                                page: pager
                            })
                        )
                    }}

                    handleFilterAssets={(data) => {
                        this.props.dispatch(
                            queryAssets({
                                q: data.q
                            })
                        )
                    }}

                    handleGetAsset={(id) => {
                        this.props.dispatch(getAsset(id))
                    }}
                />

                <MonitorModal
                    handleSubmit={() => {
                        this.props.dispatch(addAssetMonitor(this.props.modalAsset.id, {
                            monitor: monitorModal.getNotifies().monitor,
                            in_notify: monitorModal.getNotifies().in_notify,
                            out_notify: monitorModal.getNotifies().out_notify,
                            in_min: parseFloat(inMin.value.trim()||0),
                            out_min: parseFloat(outMin.value.trim()||0),
                            email: notifyEmail.value.trim()
                        }))
                    }}
                    isMonitorUpdated={this.props.isMonitorUpdated}
                    defaultNotifyEmail={this.props.defaultNotifyEmail}
                    asset={this.props.modalAsset||{}}
                    modal={node => modal = node}
                    monitor={node => monitor = node}
                    inMin={node => inMin = node}
                    outMin={node => outMin = node}
                    notifyEmail={node => notifyEmail = node}
                    ref={node => monitorModal = node}

                    handleChangeControlledComponent={(e) => {
                        this.props.dispatch(changeControlledComponent("modalAsset.condition", e.target))
                    }}
                    handleChangeControlledComponent2={(e) => {
                        console.log("handle controlled component 2:", e)
                        this.props.dispatch(changeControlledComponent("modalAsset", e.target))
                    }}
                />
            </div>
        )
    }
})
