import { get } from "../../middleware/api"

export default (data) => {
    return dispatch => {
        dispatch(getDistributionRequest)
        get(`/mining/distribution?timespan=${data.timespan}`)
        .then(json => {
            dispatch(getDistributionSuccess(json))
        }, err => {
            dispatch(getDistributionFailure({
                status: "warning",
                message: "哎呀，出现了一些错误，请再试一次。"
            }))
        })
    }
}

const getDistributionRequest = {
    type: "GET_DISTRIBUTION_REQUEST",
    payload: {
        isFetching: true
    }
}

function getDistributionSuccess(json) {
    return {
        type: "GET_DISTRIBUTION_SUCCESS",
        payload: {
            isFetching: false,
            distribution: json.data
        }
    }
}

function getDistributionFailure(message) {
    return {
        type: "GET_DISTRIBUTION_FAILURE",
        payload: {
            message,
            isFetching: false
        }
    }
}
