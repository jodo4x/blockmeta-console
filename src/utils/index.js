import _cookie from "cookie"

export const cookie = {
    get: (key) => (_cookie.parse(document.cookie)[key]),

    set: (key, value, option) =>
        document.cookie = _cookie.serialize(key, value, option),

    remove: (arg) =>
        // FIXME where is `this`?
        arg.forEach && arg.forEach(value =>
           cookie.set(value, "", null)
        ) || cookie.set(arg, "", null)
}

export const assign = Object.assign

// FIXME
export function assignOnlyTrueValue() {
    let target = arguments[0], i = 1, source
    while(source = arguments[i++]) {
        for (let prop in source) {
            if (source[prop]) {
                target[prop] = source[prop]
            }
        }
    }
    return target
}
