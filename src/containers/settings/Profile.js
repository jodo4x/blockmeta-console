import React, { Component } from "react"
import { connect } from "react-redux"
import uploadAvatar from "../../actions/settings/uploadAvatar"
import getSMSCode from "../../actions/settings/getSMSCode"
import updateMobile from "../../actions/settings/updateMobile"
import updateProfile from "../../actions/settings/updateProfile"
import changeControlledComponent from "../../actions/changeControlledComponent"
import Avatar from "../../components/settings/Avatar"
import ProfileContent from "../../components/settings/ProfileContent"

const Profile = ({ profile, dispatch }) => (
    <div className="tab-pane active profile">
        <div className="panel panel-default">
            <div className="panel-body" style={{ paddingTop: 60, paddingBottom: 60 }}>
                <div className="row">
                    <div className="col-sm-3">
                        <Avatar
                            profile={profile}
                            uploadAvatar={e =>
                                dispatch(uploadAvatar(e.target.files[0]))
                            }
                        />
                    </div>
                    <div className="col-sm-9">
                        <ProfileContent
                            profile={profile}
                            handleChange={e =>
                                dispatch(changeControlledComponent("profile", e.target))
                            }
                            getSMSCode={data =>
                                dispatch(getSMSCode(data))
                            }
                            updateMobile={data =>
                                dispatch(updateMobile(data))
                            }
                            handleSubmit={data =>
                                dispatch(updateProfile(data))
                            }
                        />
                    </div>
                </div>
            </div>
        </div>
    </div>
)

export default connect(
    state => ({
        profile: state.profile
    })
)(Profile)
