import { get } from "../../middleware/api"
import {
    LOAD_MY_ASSETS_REQUEST,
    LOAD_MY_ASSETS_SUCCESS,
    LOAD_MY_ASSETS_FAILURE
} from "../../constants/actionTypes"

export default () => {
    return dispatch => {
        // dispatch(loadMyAssetsRequest)

        Promise.all([
            get("/assets/stat"),
            get("/assets")
        ])
        .then(json => {
            dispatch(loadMyAssetsSuccess(json))
        }, err => {
            dispatch(loadMyAssetsFailure({
                status: "warning",
                message: "哎呀，获取资源出错，请再试一次"
            }))
        })
    }
}

const loadMyAssetsRequest = {
    type: LOAD_MY_ASSETS_REQUEST
}

function loadMyAssetsSuccess(json) {
    return {
        type: LOAD_MY_ASSETS_SUCCESS,
        payload: {
            statistics: json[0],
            assets: json[1].data
        }
    }
}

function loadMyAssetsFailure(message) {
    return {
        type: LOAD_MY_ASSETS_FAILURE,
        payload: {
            message
        }
    }
}
