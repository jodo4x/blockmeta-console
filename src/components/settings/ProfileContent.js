import React from "react"
import COUNTRY_LIST from "../../constants/countryList"
import Mobile from "./Mobile"

const ProfileContent = ({
        profile,
        handleChange,
        getSMSCode,
        updateMobile,
        handleSubmit }) => {
    let nickname, country, address

    return profile ? (
        <form onSubmit={e => {
            e.preventDefault()
            handleSubmit({
                nickname: nickname.value.trim(),
                country: country.value,
                address: address.value.trim()
            })
        }}>
            <div className="form-group">
                <label className="control-label" htmlFor="field__email">邮箱</label>
                <input className="form-control" id="field__email" type="text" value={profile.email} disabled={true} style={{backgroundColor: "#fff"}} />
                <p className="help-block" style={{ fontSize: 12, marginTop: 8 }}>已注册后，邮箱不可更改</p>
            </div>

            <div className="form-group">
                <label className="control-label" htmlFor="field__nickname">昵称</label>
                <input
                    className="form-control input-lg"
                    id="field__nickname"
                    type="text"
                    value={profile.nickname||""}
                    ref={node => nickname = node}
                    onChange={handleChange}
                />
            </div>

            <div className="form-group">
                <label className="control-label" htmlFor="field__country">地址</label>
                <div className="">
                    <select
                        className="form-control input-lg"
                        id="field__country"
                        value={profile.country||"CN"}
                        ref={node => country = node}
                        style={{ marginBottom: 20 }}
                        onChange={handleChange}
                    >
                        {COUNTRY_LIST.map((country, index) => (
                            <option value={country.code} key={index}>{country.cn}</option>
                        ))}
                    </select>
                    <input
                        className="form-control input-lg"
                        id="field__address"
                        type="text"
                        value={profile.address||""}
                        style={{ width: 458 }}
                        ref={node => address = node}
                        onChange={handleChange}
                    />
                </div>
            </div>

            <Mobile
                profile={profile}
                handleChange={handleChange}
                getSMSCode={getSMSCode}
                updateMobile={updateMobile}
            />

            <button className="btn btn-success btn-lg" type="submit" style={{ marginTop: 40 }}>保存</button>
        </form>
    ) : null
}

export default ProfileContent
