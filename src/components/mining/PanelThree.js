import React from "react"
import DatePicker from "react-datepicker"
import Select from "react-select"
import moment from "moment"

import { get } from "../../middleware/api"

export default class PanelThree extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            // start_time: moment(),
            // end_time: moment(),
            curr_currency: "CNY",
            curr_price: "",
            current_difficulty: "",
            difficulty_increase: "",
            difficulty_increase_cycle: "",

            // defaults
            defaults: {
                current_difficulty: "",
                difficulty_increase: "",
                difficulty_increase_cycle: "",
                curr_currency: "CNY",
                curr_price: ""
            }
        }

        this.handleStartDateChange = this.handleStartDateChange.bind(this)
        this.handleEndDateChange = this.handleEndDateChange.bind(this)
        this.handleBlur = this.handleBlur.bind(this)
        this.handleResetForm = this.handleResetForm.bind(this)
    }

    componentDidMount() {
        get("/meta/markets")
        .then(json => {
            this.setState({
                current_difficulty: json.data.current_difficulty,
                difficulty_increase: (json.data.difficulty_increase*100).toFixed(0),
                difficulty_increase_cycle: json.data.difficulty_increase_cycle,
                priceMap: json.data.price,
                curr_price: json.data.price[this.state.curr_currency],
                defaults: {
                    current_difficulty: json.data.current_difficulty,
                    difficulty_increase: (json.data.difficulty_increase*100).toFixed(0),
                    difficulty_increase_cycle: json.data.difficulty_increase_cycle,
                    curr_currency: "CNY",
                    curr_price: json.data.price[this.state.curr_currency]
                }
            })
        })
    }

    handleStartDateChange(date) {
        this.setState({
            start_time: date
        })
    }

    handleEndDateChange(date) {
        this.setState({
            end_time: date
        })
    }

    handleBlur(e) {
        e.target.classList.remove("has-err")
    }

    handleResetForm() {
        this.setState({
            current_difficulty: this.state.defaults.current_difficulty,
            difficulty_increase: this.state.defaults.difficulty_increase,
            difficulty_increase_cycle: this.state.defaults.difficulty_increase_cycle,
            curr_price: this.state.defaults.curr_price,
            curr_currency: this.state.defaults.curr_currency,
            start_time: null,
            end_time: null

        })

        this.refs.inpt4.value =
        this.refs.inpt5.value =
        this.refs.inpt6.value =
        this.refs.inpt7.value =
        this.refs.inpt9.value =
        this.refs.inpt10.value = ""
    }

    render() {
        const { errDetails } = this.props

        let symbols = {
            "CNY": "¥",
            "USD": "$",
            "EUR": "€"
        }

        return (
            <div className="panel panel-default calculator">
                <div className="panel-heading">
                    <h2 className="panel-title"><span>收益</span>计算器</h2>
                </div>

                <div className="panel-body">
                    {/* calculator form start */}
<form onSubmit={(e) => {
    e.preventDefault()

    let { inpt4, inpt5, inpt6, inpt7, inpt9, inpt10 } = this.refs
    const _sd = this.state.start_time ? this.state.start_time._d : null
    const _ed = this.state.end_time ? this.state.end_time._d : null

    this.props.calculateMining({
        current_difficulty: this.state.current_difficulty,
        difficulty_increase: this.state.difficulty_increase/100,
        difficulty_increase_cycle: this.state.difficulty_increase_cycle,
        start_time: this.state.start_time ? `${_sd.getFullYear()}-${_sd.getMonth()+1}-${_sd.getDate()}`: "",
        end_time: this.state.end_time ? `${_ed.getFullYear()}-${_ed.getMonth()+1}-${_ed.getDate()}` : "",
        mm_price: inpt4.value.trim(),
        mm_power: inpt5.value.trim(),
        electricity_costs: inpt6.value.trim(),
        mm_hash_rate: inpt7.value.trim(),
        conversion_rate: this.state.curr_price,
        mine_pool_fee: inpt9.value.trim()/100,
        mm_extras: inpt10.value.trim()/100
    })
}}>
    <table className="table t1">
        <tbody>
            <tr>
                <td style={{ paddingBottom: 20 }}>
                    <h3 className="td-title">1.计算难度</h3>
                    <table className="t2">
                        <tbody>
                            <tr>
                                <td><label>计算难度</label></td>
                                <td><input
                                    onBlur={this.handleBlur}
                                    className={errDetails.inpt1 && "has-err"}
                                    type="text"
                                    style={{ width: 150 }}
                                    ref="inpt1"
                                    value={this.state.current_difficulty}
                                    onChange={(e) => {
                                        this.setState({
                                            current_difficulty: e.target.value.trim()
                                        })
                                    }}
                                /></td>
                            </tr>
                            <tr>
                                <td><label>计算难度增幅</label></td>
                                <td><input
                                    onBlur={this.handleBlur}
                                    className={errDetails.inpt2 ? "text-center has-err" : "text-center"}
                                    type="text"
                                    ref="inpt2"
                                    style={{ width: 56 }}
                                    value={this.state.difficulty_increase}
                                    onChange={(e) => {
                                        this.setState({
                                            difficulty_increase: e.target.value.trim()
                                        })
                                    }}
                                /><span style={{ marginLeft: 5 }}>%</span></td>
                            </tr>
                            <tr>
                                <td><label>计算调整周期</label></td>
                                <td><input
                                    onBlur={this.handleBlur}
                                    className={errDetails.inpt3 ? "text-center has-err" :"text-center"}
                                    type="text"
                                    ref="inpt3"
                                    style={{ width: 56 }}
                                    value={this.state.difficulty_increase_cycle}
                                    onChange={(e) => {
                                        this.setState({
                                            difficulty_increase_cycle: e.target.value.trim()
                                        })
                                    }}
                                /><span style={{ marginLeft: 5 }}>天</span></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td style={{ paddingBottom: 20 }}>
                    <h3 className="td-title">2.挖矿时间</h3>
                    <table className="t2">
                        <tbody>
                            <tr>
                                <td><label>开始时间</label></td>
                                <td>
                                    <DatePicker
                                        dateFormat="YYYY-MM-DD"
                                        placeholderText="选择开始日期"
                                        minDate={moment()}
                                        className={errDetails.start_time ? "datepicker has-err" : "datepicker"}
                                        selected={this.state.start_time}
                                        onChange={this.handleStartDateChange}
                                        onBlur={this.handleBlur}
                                    />
                                </td>
                            </tr>
                            <tr>
                                <td><label>结束时间</label></td>
                                <td>
                                    <DatePicker
                                        dateFormat="YYYY-MM-DD"
                                        placeholderText="选择结束日期"
                                        minDate={moment().add(1, "days")}
                                        className={errDetails.end_time ? "datepicker has-err" : "datepicker"}
                                        selected={this.state.end_time}
                                        onChange={this.handleEndDateChange}
                                        onBlur={this.handleBlur}
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <h3 className="td-title">3.硬件条件</h3>
                    <table className="t2">
                        <tbody>
                            <tr>
                                <td><label>设备成本</label></td>
                                <td>
                                    <table className="table" style={{ marginBottom: 0 }}>
                                        <tbody>
                                            <tr>
                                                <td style={{ padding: "5px 0 0" }}><input onBlur={this.handleBlur} className={errDetails.inpt4 && "has-err"} type="text" ref="inpt4" style={{ width: 56 }} /></td>
                                                <td style={{ paddingBottom: 0 }}>
                                                    <Select
                                                        options={
                                                            [{label:"美元", value:"USD"},
                                                             {label:"人民币",value:"CNY"}].map((item, index) => {
                                                                return {
                                                                    label: item.label,
                                                                    value: item.value
                                                                }
                                                            })
                                                        }
                                                        value={this.state.curr_currency}
                                                        clearable={false}
                                                        searchable={false}
                                                        onChange={(selected) => {
                                                            this.setState({
                                                                curr_currency: selected.value,
                                                                curr_price: this.state.priceMap[selected.value]
                                                            })
                                                        }}
                                                    />
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td><label>设备功率</label></td>
                                <td><input onBlur={this.handleBlur} className={"text-center " + (errDetails.inpt5 && "has-err")} type="text" ref="inpt5" style={{ width: 56 }} /><span style={{ marginLeft: 5 }}>W</span></td>
                            </tr>
                            <tr>
                                <td><label>电费</label></td>
                                <td><input onBlur={this.handleBlur} className={"text-center " + (errDetails.inpt6 && "has-err")} type="text" ref="inpt6" style={{ width: 56 }} /><span style={{ marginLeft: 5 }}>{symbols[this.state.curr_currency]}&#47;度</span></td>
                            </tr>
                            <tr>
                                <td><label>计算力</label></td>
                                <td><input onBlur={this.handleBlur} className={"text-center " + (errDetails.inpt7 && "has-err")} type="text" ref="inpt7" style={{ width: 56 }} /><span style={{ marginLeft: 5 }}>TH&#47;s</span></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
                <td>
                    <h3 className="td-title">4.其他</h3>
                    <table className="t2">
                        <tbody>
                            <tr>
                                <td><label>比特币汇率</label></td>
                                <td><input
                                    onBlur={this.handleBlur}
                                    className={errDetails.inpt8 && "has-err"}
                                    type="text"
                                    ref="inpt8"
                                    value={this.state.curr_price}
                                    onChange={(e) => {
                                        this.setState({
                                            curr_price: e.target.value.trim()
                                        })
                                    }}
                                /><span style={{ marginLeft: 5 }}>{symbols[this.state.curr_currency]}&#47;B</span></td>
                            </tr>
                            <tr>
                                <td><label>矿池手续费</label></td>
                                <td><input onBlur={this.handleBlur} className={errDetails.inpt9 && "has-err"} type="text" ref="inpt9" /><span style={{ marginLeft: 5 }}>%</span></td>
                            </tr>
                            <tr>
                                <td><label>杂项手续费</label></td>
                                <td><input onBlur={this.handleBlur} className={errDetails.inpt10 && "has-err"} type="text" ref="inpt10" /><span style={{ marginLeft: 5 }}>{symbols[this.state.curr_currency]}</span></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td></td>
                <td className="text-left" style={{ paddingTop: 10, paddingBottom: 20 }}>
                    <button className="btn btn-lg btn-success" type="submit">计算收益</button>
                    <button className="btn btn-lg btn-default" style={{ color: "#5d6577", fontSize: 14, marginLeft: 20, padding: "8px 20px", border: "1px solid #ddd" }} type="button" onClick={this.handleResetForm}>重置</button>
                </td>
            </tr>
        </tbody>
    </table>
</form>
                    {/* calculator form end */}
                </div>
            </div>
        )
    }
}
