import React from "react"
import echarts from "echarts"

import { connect } from "react-redux"
import getMiningInfo from "../../actions/mining/getMiningInfo"

const Loading = connect(
    state => ({
        isFetching: state.spinners.miningInfoSpinner
    })
)((props) => {
    return props.isFetching ? <span className="myLoading">正在加载...</span> : null
})

class PanelTwo extends React.Component {
    constructor(props) {
        super(props)
        this.handleTabClick = this.handleTabClick.bind(this)
    }

    componentDidMount() {
        this.props.dispatch(getMiningInfo({
            timespan: "1m"
        }))
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps !== this.props) {
            const mInfo = nextProps.miningInfo.data

            if (mInfo) {
                let diffMin = Math.min.apply(null, mInfo.difficulty.data.map(el => el[0]))*0.95
                let diffMax = Math.max.apply(null, mInfo.difficulty.data.map(el => el[0]))*1.05
                let rateMin = Math.min.apply(null, mInfo.hash_rate.data.map(el => el[0]))*0.95
                let rateMax = Math.max.apply(null, mInfo.hash_rate.data.map(el => el[0]))*1.05

                const option = {
                    tooltip: {
                        trigger: "axis",
                        borderRadius: 1,
                        backgroundColor: "#fff",
                        formatter: (params) => {
                            var res = `
                            <div class="axispointer">
                                <div class="datetime">${params[0].name}</div>
                                <div style="font-size: 12px;">
                                    <div><span class="d1">难度(百万):</span>${params[0].value}</div>
                                    <div><span class="d2">算力(TH/s):</span>${params[1].value}</div>
                                </div>
                            </div>
                            `
                            return res
                        },
                        textStyle: {
                            color: "#1e294a"
                        },
                        axisPointer: {
                            type: "line",
                            lineStyle: {
                                color: "#e9e9e9",
                                width: 1
                            }
                        }
                    },
                    legend: {
                        // data: Object.keys(nextProps.miningInfo.data||{}).map(el => el)
                        data: [ "难度", "算力" ]
                    },
                    grid: {
                        left: 30,
                        right: 30,
                        bottom: 30,
                        y: 10,
                        containLabel: false
                    },
                    color: [ "#39ca74", "#688fd4" ],
                    xAxis: [
                        {
                            type: "category",
                            boundaryGap: false,
                            data: (function(data) {
                                let ret
                                Object.keys(data).forEach((el, index) => {
                                    ret = data[el].data.map((arr) => {
                                        const d = new Date(arr[1]*1000)
                                        return `${d.getFullYear()}/${d.getMonth()+1}/${d.getDate()}`
                                    })
                                })
                                return ret
                            }(nextProps.miningInfo.data||{})),
                            splitLine: false,
                            axisLine: {
                                lineStyle: {
                                    color: "#f2f4f7",
                                    width: 1
                                }
                            },
                            axisTick: {
                                color: "#ddd",
                                lineStyle: {
                                    color: "#f1f4f7",
                                    width: 1
                                }
                            },
                            axisLabel: {
                                margin: 15,
                                textStyle: {
                                    color: "#5d6577"
                                }
                            }
                        }
                    ],
                    yAxis: [
                        {
                            type: "value",
                            name: "难度",
                            scale: true,
                            min: diffMin,
                            max: diffMax,
                            // splitLine: false,
                            splitLine: {
                                onGap: true,
                                lineStyle: {
                                    color: "#f2f4f7"
                                }
                            },
                            axisLine: { show: false },
                            axisTick: { show: false },
                            axisLabel: { show: false }
                        },
                        {
                            type: "value",
                            name: "算力",
                            scale: true,
                            min: rateMin,
                            max: rateMax,
                            splitLine: false,
                            axisLine: { show: false },
                            axisTick: { show: false },
                            axisLabel: { show: false }
                        }
                    ],
                    series: [
                        {
                            name: "难度",
                            type: "line",
                            data: (function(data) {
                                return data.difficulty &&
                                data.difficulty.data &&
                                data.difficulty.data.map((arr) => {
                                    return arr[0]
                                })
                            }(mInfo||{})),
                            itemStyle: {
                                normal: {
                                    lineStyle: {
                                        color: "#39ca74",
                                        width: 1
                                    }
                                },
                                emphasis: {
                                    color: "#39ca74"
                                }
                            }
                        },
                        {
                            name: "算力",
                            type: "line",
                            yAxisIndex: 1,
                            data: (function(data) {
                                return data.hash_rate &&
                                data.hash_rate.data &&
                                data.hash_rate.data.map((arr) => {
                                    return arr[0]
                                })
                            }(mInfo||{})),
                            itemStyle: {
                                normal: {
                                    lineStyle: {
                                        color: "#6890d4",
                                        width: 1
                                    }
                                },
                                emphasis: {
                                    color: "#6890d4"
                                }
                            }
                        }
                    ]
                }

                const lineChart = echarts.init(document.getElementById("J__lineChart"))
                lineChart.setOption(option)
            }
        }
    }

    handleTabClick(e) {
        const parent = e.target.parentElement.parentElement // `<ul>`
        const siblings = parent.children // `<li>...`

        ;[].forEach.call(siblings, (el) => {
            el.classList.remove("active")
        })
        e.target.parentElement.classList.add("active")
    }

    render() {
        let { push } = this.context.router
        let { dispatch, miningInfo } = this.props

        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    <ul className="nav nav-pills pull-right p2-nav" ref="p2tabs">
                        <li className="active">
                            <a
                                href="javascript:;"
                                onClick={(e) => {
                                    // push({ pathname: "/mining", query: { p2timespan: "1m" } })
                                    this.handleTabClick(e)
                                    dispatch(getMiningInfo({ timespan: "1m" }))
                                }}
                            >
                                一个月
                            </a>
                        </li>
                        <li>
                            <a
                                href="javascript:;"
                                onClick={(e) => {
                                    // push({ pathname: "/mining", query: { p2timespan: "3m" } })
                                    this.handleTabClick(e)
                                    dispatch(getMiningInfo({ timespan: "3m" }))
                                }}
                            >
                                三个月
                            </a>
                        </li>
                        <li>
                            <a
                                href="javascript:;"
                                onClick={(e) => {
                                    // push({ pathname: "/mining", query: { p2timespan: "6m" } })
                                    this.handleTabClick(e)
                                    dispatch(getMiningInfo({ timespan: "6m" }))
                                }}
                            >
                                半年
                            </a>
                        </li>
                        <li>
                            <a
                                href="javascript:;"
                                onClick={(e) => {
                                    // push({ pathname: "/mining", query: { p2timespan: "1y" } })
                                    this.handleTabClick(e)
                                    dispatch(getMiningInfo({ timespan: "1y" }))
                                }}
                            >
                                最近一年
                            </a>
                        </li>
                        <li>
                            <a
                                href="javascript:;"
                                onClick={(e) => {
                                    // push({ pathname: "/mining", query: { p2timespan: "all" } })
                                    this.handleTabClick(e)
                                    dispatch(getMiningInfo({ timespan: "all" }))
                                }}
                            >
                                所有
                            </a>
                        </li>
                    </ul>
                    <h2 className="panel-title"><span>难度</span>和算力</h2>
                </div>

                <div className="panel-body clearfix">
                    <table className="table">
                        <tbody>
                            <tr>
                                <td align="middle" style={{ width: "28%", verticalAlign: "middle", borderTop: 0 }}>
                                    {miningInfo.current_difficulty ? (
                                        <div>
                                            <div style={{ marginBottom: 10 }}>
                                                <span style={{ color: "#1e294a", marginRight: 7 }}>当前难度:(百万)</span>
                                                <span style={{ color: "#39ca74", fontSize: 20 }}>{miningInfo.current_difficulty}</span>
                                            </div>
                                            <div>
                                                <span style={{ color: "#1e294a", marginRight: 7 }}>当前算力:(PH/s)</span>
                                                <span style={{ color: "#39ca74", fontSize: 20 }}>{miningInfo.current_hash_rate}</span>
                                            </div>
                                        </div>
                                    ) : null}
                                </td>
                                <td align="middle" style={{ width: "72%", borderTop: 0 }}>
                                    <div className="pull-right" id="J__lineChart" style={{ width: "100%", height: 300-36 }}></div>
                                    <Loading />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        )
    }
}

PanelTwo.contextTypes = {
    router: React.PropTypes.object.isRequired
}

export default connect(
    state => ({
        miningInfo: state.miningInfo
    })
)(PanelTwo)
