import {
    CHANGE_CONTROLLED_COMPONENT
} from "../constants/actionTypes"

export default (statePropName, elem) => {
    let value
    if (elem.type === "checkbox") {
        value = elem.checked
    } else {
        value = elem.value
    }

    return {
        type: CHANGE_CONTROLLED_COMPONENT,
        payload: {
            statePropName,
            target: {
                name: elem.id.replace(/field[\_]*/, ""),
                value: value/*NOTE: please DO NOT trim value*/
            }
        }
    }
}
