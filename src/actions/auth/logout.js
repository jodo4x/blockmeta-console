import { cookie } from "../../utils"
import { push } from "react-router-redux"
import {
    LOGOUT_REQUEST,
    LOGOUT_SUCCESS,
    LOGOUT_FAILURE
} from "../../constants/actionTypes"

export default () => {
    return dispatch => {
        dispatch(logoutRequest)
        // Fake loading...
        let timeoutId = setTimeout(() => {
            clearToken()
            dispatch(logoutSuccess)
            dispatch(push("/login"))
            clearTimeout(timeoutId)
        }, 2000)
    }
}

const logoutRequest = {
    type: LOGOUT_REQUEST
}

const logoutSuccess = {
    type: LOGOUT_SUCCESS
}

function clearToken() {
    cookie.remove(["_at_", "_rt_"])
}
