import React, { Component } from "react"
import { connect } from "react-redux"
import { Link } from "react-router"
import queryNotifications from "../../actions/notifications/queryNotifications"
import deleteNotification from "../../actions/notifications/deleteNotification"
import toggleNotification from "../../actions/notifications/toggleNotification"
import toggleNotificationAll from "../../actions/notifications/toggleNotificationAll"
import toggleNotificationDisplay from "../../actions/notifications/toggleNotificationDisplay"
import getNotification from "../../actions/notifications/getNotification"
import batchMarkNotifications from "../../actions/notifications/batchMarkNotifications"
import batchDeleteNotifications from "../../actions/notifications/batchDeleteNotifications"

export default connect(
    state => ({
        notifications: state.notifications
    })
)(({ notifications, dispatch }) =>
    notifications ? (
        <div className="notifications-container">
            <div className="panel panel-default">
                <div className="panel-heading">
                    <h3 className="panel-title">消息通知</h3>
                </div>
                <div className="panel-body">
                    <div className="clearfix toolbar">
                        <label htmlFor="field__selectAll">
                            <input
                                type="checkbox"
                                id="field__selectAll"
                                checked={!!notifications.isAllSelected}
                                onChange={e => dispatch(toggleNotificationAll({
                                    isAllSelected: e.target.checked,
                                    items: notifications.items
                                })) }
                            />
                            <span style={{ color: "#1e294a" }}>全选</span>
                        </label>
                        <div className="btn-group">
                            <div className="btn-group">
                                <button
                                    className="btn btn-default"
                                    type="button"
                                    disabled={!(notifications.batchItems && !!notifications.batchItems.length)}
                                    onClick={() => dispatch(batchMarkNotifications(notifications.batchItems)) }
                                >
                                    <span>标记已读</span>
                                </button>
                            </div>
                            <div className="btn-group">
                                <button
                                    className="btn btn-default"
                                    type="button"
                                    disabled={!(notifications.batchItems && !!notifications.batchItems.length)}
                                    onClick={() => dispatch(batchDeleteNotifications(notifications.batchItems)) }
                                >
                                    <span>删除</span>
                                </button>
                            </div>
                        </div>
                        <div className="pull-right">
                            <span>
                                <button
                                    className="btn btn-link"
                                    type="button"
                                    disabled={!notifications.has_prev}
                                    onClick={e =>
                                        dispatch(queryNotifications(--notifications.page))
                                    }
                                >
                                    <i className="fa fa-chevron-left"></i>
                                </button>
                                <button
                                    className="btn btn-link"
                                    type="button"
                                    disabled={!notifications.has_next}
                                    onClick={e =>
                                        dispatch(queryNotifications(++notifications.page))
                                    }
                                >
                                    <i className="fa fa-chevron-right"></i>
                                </button>
                            </span>
                            <span className="hidden" style={{ marginRight: 15 }}>第1-50封</span>
                            <span>共{notifications.total}封</span>
                        </div>
                    </div>
                    {notifications.items.length ? (
                        <ul className="list-unstyled notifications">
                            {notifications.items && notifications.items.map((item, index) => (
                                <li className={"clearfix" + (!item.is_read && " unread" || "") + (item.isShown && " open" || "")} key={index}>
                                    <div className="row">
                                        <div className="col-xs-1">
                                            <input
                                                className="pull-left"
                                                type="checkbox"
                                                name={"field__checkbox-" + item.id}
                                                checked={item.isChecked}
                                                onChange={() => dispatch(toggleNotification(item.id))}
                                            />
                                        </div>
                                        <div className="col-xs-3">
                                            {item.is_read ? (
                                                <a
                                                    href="javascript:;"
                                                    onClick={() => dispatch(toggleNotificationDisplay(item.id))}
                                                >
                                                    {item.category}
                                                </a>
                                            ) : (
                                                <a
                                                    href="javascript:;"
                                                    onClick={() => dispatch(getNotification(item.id))}
                                                >
                                                    {item.category}
                                                </a>
                                            )}
                                        </div>
                                        <div className="col-xs-6">
                                            <div className="pull-left text-summary">{item.title}</div>
                                        </div>
                                        <div className="col-xs-2 text-right">
                                            <button
                                                className="btn btn-link pull-right hidden"
                                                onClick={() => dispatch(deleteNotification(item.id)) }
                                            >
                                                <span>删除</span>
                                            </button>
                                            <span>{new Date(item.create_time).getMonth()+1}月{new Date(item.create_time).getDate()}日</span>
                                        </div>
                                    </div>

                                    <div className="notification-body clearfix">
                                        <div className="row">
                                            <div className="col-xs-10 col-xs-offset-1" style={{ padding: "0 6px" }}>
                                                <div dangerouslySetInnerHTML={{ __html: item.body }} />
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    ) : <div className="well text-center" style={{ marginTop: 20 }}>没有相关通知</div>}
                </div>
            </div>
        </div>
    ) : null
)
