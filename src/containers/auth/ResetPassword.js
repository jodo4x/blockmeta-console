import React, { PropTypes } from "react"
import { connect } from "react-redux"
import resetPassword from "../../actions/auth/resetPassword"
import Field from "../../components/form/Field"

const ResetPassword = ({ location, dispatch }) => {
    let pwd1, pwd2

    return (
        <div className="panel panel-default">
            <div className="panel-body">
                <form onSubmit={e => {
                    e.preventDefault()

                    dispatch(resetPassword({
                        code: location.query.code,
                        new_password_1: pwd1.value.trim(),
                        new_password_2: pwd2.value.trim()
                    }))
                }}>
                    <legend className="text-center text-capitalize">Blockmeta</legend>
                    <Field
                        name="pwd1"
                        label="新密码"
                        type="password"
                        placeholder="请输入新密码"
                        _ref={node => pwd1 = node}
                    >
                    </Field>
                    <Field
                        name="pwd2"
                        label="确认密码"
                        type="password"
                        placeholder="请再次输入新密码"
                        _ref={node => pwd2 = node}
                    />
                    <button className="btn btn-success btn-block btn-lg text-capitalize" type="submit">保存新密码</button>
                </form>
            </div>
        </div>
    )
}

export default connect()(ResetPassword)
