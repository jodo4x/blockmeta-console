import React, { PropTypes } from "react"
import { connect } from "react-redux"
import updatePassword from "../../actions/settings/updatePassword"

const Password = ({ dispatch }) => {
    let pass1, pass2, pass3

    return (
        <div className="tab-pane active password">
            <form onSubmit={e => {
                e.preventDefault()

                dispatch(updatePassword({
                    new_password_1: pass1.value.trim(),
                    new_password_2: pass2.value.trim(),
                    old_password: pass3.value.trim()
                }))
                pass1.value = pass2.value = pass3.value = ""
            }}>
                <div className="panel panel-default">
                    <div className="panel-body" style={{ paddingTop: 120, paddingBottom: 120 }}>
                        <div className="row">
                            <div className="col-sm-9 col-sm-offset-3">
                                <div style={{ width: 390 }}>
                                    <div className="form-group">
                                        <label className="control-label" htmlFor="field__pass3">旧密码</label>
                                        <input className="form-control input-lg" id="field__pass3" type="password" ref={node => pass3 = node} placeholder="请输入旧密码" />
                                    </div>

                                    <div className="form-group">
                                        <label className="control-label" htmlFor="field__pass1">新密码</label>
                                        <input className="form-control input-lg" id="field__pass1" type="password" ref={node => pass1 = node} placeholder="请输入新密码" />
                                    </div>

                                    <div className="form-group">
                                        <label className="control-label" htmlFor="field__pass2">确认新密码</label>
                                        <input className="form-control input-lg" id="field__pass2" type="password" ref={node => pass2 = node} placeholder="请再次输入新密码" />
                                    </div>
                                    <button className="btn btn-success btn-lg" type="submit">保存</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default connect()(Password)
