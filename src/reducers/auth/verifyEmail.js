import {
    VERIFY_EMAIL_SUCCESS,
    VERIFY_EMAIL_FAILURE
} from "../../constants/actionTypes"
import { assign } from "../../utils"

export default (state, { type, payload }) => {
    switch(type) {
        case VERIFY_EMAIL_SUCCESS:
        case VERIFY_EMAIL_FAILURE:
            return assign({}, state, {
                message: payload.message
            })
        default:
            return state
    }
}
