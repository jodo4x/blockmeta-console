import React from "react"
import addressTypes from "../../constants/addressTypes"
import ReactTooltip from "react-tooltip"
import echarts from "echarts"
import moment from "moment"

moment.locale("zh-cn")

import { connect } from "react-redux"
import getAssetsState from "../../actions/assets/getState"

export default connect(
    state => ({
        statistics: state.statistics
    })
)(class Overview extends React.Component {
    componentDidMount() {
        // this.props.dispatch(getAssetsState())
    }

    componentWillReceiveProps(nextProps) {
        const { statistics } = nextProps

        if (statistics) {
            const option = {
                tooltip: {
                    trigger: "item",
                    formatter: "{b}: {c} <sup>({d}%)</sup>"
                },
                legend: {
                    orient: "vertical",
                    x: "right",
                    data: Object.keys(statistics.proportion).map((key) => {
                        return addressTypes.filter(el => el.key === key)[0].name
                    }),
                    textStyle: { color: "#5d6577" },
                    itemWidth: 16,
                    itemHeight: 16
                },
                color: [
                    "#6890d4", "#68d4b2", "#e6f3a4", "#a4e59b", "#54becc"
                ],
                series: [
                    {
                        type: "pie",
                        radius: ["40%", "65%"],
                        data: (function() {
                            const ret = []
                            for (let p in statistics.proportion) {
                                ret.push({
                                    name: addressTypes.filter(el => el.key === p)[0].name,
                                    value: statistics.proportion[p] + ""
                                })
                            }
                            return ret
                        }()),
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 0,
                                shadowOffsetX: 0,
                                shadowColor: "rgba(0,0,0,0.5)"
                            }
                        }
                    }
                ]
            }

            let pieChart = echarts.init(document.getElementById("J__piechart"))
            pieChart.setOption(option)
        }
    }

    render() {
        let { statistics } = this.props

        let d  // 不能放在 `if {}`内

        if (statistics && statistics.last_visit_assets_time) {
            d = new Date(statistics.last_visit_assets_time)
        }
        return (
            <div style={{ position: "relative", height: 300, marginBottom: 15 }}>
                <div className="panel panel-default" style={{ width: "55%", marginBottom: 0, position: "absolute", left: 0, top: 0, bottom: 0 }}>
                    <div className="panel-heading">
                        <h3 className="panel-title">资产概况</h3>
                    </div>
                    <div className="panel-body r1">
                        <div className="">
                            <div className="">
                                {statistics && (
                                    <div>
                                        <div className="row">
                                            <div className="col-sm-6">
                                                <dl>
                                                    <dt>资产总额</dt>
                                                    <dd>
                                                        <span className="d1">{statistics.all_amount} <strong style={{ fontSize: 14, color: "#828aa0" }}>BTC</strong></span>
                                                        <span className="d2">
                                                            {statistics.new_amount}
                                                            <sup data-tip data-for="fa1"><i className="fa fa-question-circle-o"></i></sup>
                                                            <ReactTooltip id="fa1">
                                                                <span>距离上次登陆的交易额度</span>
                                                            </ReactTooltip>
                                                        </span>
                                                    </dd>
                                                </dl>
                                            </div>
                                            <div className="col-sm-6">
                                                <dl>
                                                    <dt>交易数</dt>
                                                    <dd>
                                                        <span className="d1">{statistics.all_transaction}</span>
                                                        <span className="d2">
                                                            +{statistics.new_transaction}笔
                                                            <sup data-tip data-for="fa2" ><i className="fa fa-question-circle-o"></i></sup>
                                                            <ReactTooltip id="fa2">
                                                                <span>距离上次登陆的交易笔数</span>
                                                            </ReactTooltip>
                                                        </span>
                                                    </dd>
                                                </dl>
                                            </div>
                                        </div>
                                        <div
                                            style={{ color: "#828aa0", fontStyle: "italic" }}
                                        >
                                            <small>距上次登录 <span>
                                            {statistics.last_visit_assets_time && 
                                            moment(statistics.last_visit_assets_time).fromNow()}
                                            </span></small>
                                        </div>
                                    </div>
                                )}
                                {statistics && (
                                    <div className="s2">
                                        <div className="clearfix">

                                            <div className="" style={{ marginBottom: 5 }}>
                                                <div className="clearfix">
                                                    <span style={{ color: "#5d6577", fontSize: 14 }}>监控配额</span>
                                                </div>
                                            </div>
                                            <div className="" style={{ paddingTop: 5, width: "65%" }}>
                                                <div className="progress" data-tip data-for="prog">
                                                    <div className={"progress-bar progress-bar-" + (statistics.already >= 8 ? "warning" : "success")} style={{ width: statistics.already/statistics.allow*100 + "%" }}></div>
                                                </div>
                                                <ReactTooltip id="prog">
                                                    <span className="pull-right">{statistics.already}/{statistics.allow}个</span>
                                                </ReactTooltip>
                                            </div>

                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                </div>
                <div style={{ width: "45%", position: "absolute", top: 0, right: 0, bottom: 0 }}>
                    <div className="panel panel-default" style={{ position: "absolute", marginBottom: 0, left: 15, top: 0, right: 0, bottom: 0 }}>
                        <div className="panel-heading">
                            <h3 className="panel-title">资产类型</h3>
                        </div>
                        <div className="panel-body r2">
                            <div id="J__piechart" style={{ width: "100%", height: 202 }}></div>
                            {(!statistics || !statistics.already) && <div className="bm-center">没有资产</div>}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
})
