import {
    RESET_MESSAGE
} from "../constants/actionTypes"

const resetMessage = {
    type: RESET_MESSAGE
}

export default resetMessage
