const urls = {
    // "development": "http://192.168.199.120:5000/api",
    // "production": "http://121.201.29.105:5000/api"
}

// const BASE_URL = "https://106.185.45.27:30000/api"
// let BASE_URL = 'http://console.blockmeta.com/api'
// TEST ENV
// let BASE_URL = 'http://106.185.45.27/api'

let BASE_URL

if (process.env.TEST_ENV === 'test') {
  BASE_URL = 'http://console.blockmeta.com/api'
} else {
  BASE_URL = 'https://console.blockmeta.com/api'
}

export default BASE_URL
