import React, { PropTypes } from "react"
import { connect } from "react-redux"
import signup from "../../actions/auth/signup"
import Field from "../../components/form/Field"

import { EMAIL_PATTERN, PASSWORD_PATTERN } from "../../constants/pattern"

export default connect(
    state => ({
        isSignedUp: state.isSignedUp
    })
)(({ isSignedUp = false, dispatch }) => {
    let emailRef, passwordRef, n1, n2

    return (
        <div className="panel panel-default">
            <div className="panel-body">
                {isSignedUp ? (
                    <div className="text-center" style={{ fontSize: 14, color: "#424f72", lineHeight: 1.65 }}>
                        <h2 className="text-capitalize text-center" style={{ color: "#1e294a", fontSize: 25, marginBottom: 20 }}>Blockmeta</h2>
                        <img src="//maoxiang-static-head.b0.upaiyun.com/tml151p081q8rq51.png" alt="" style={{ width: 70, marginBottom: 35 }} />
                        <p style={{ fontSize: 14 }}>我们已将确认邮件发送至<span style={{ color: "" }}>你的邮箱</span>
                        <br />点击邮件中的链接进行下一步操作</p>
                        <hr />
                        <div className="text-center">
                            <a href="https://blockmeta.com">返回首页</a>
                        </div>
                    </div>
                ) : (
                    <form onSubmit={e => {
                        e.preventDefault()

                        dispatch(signup({
                            email: emailRef.value.trim(),
                            password: passwordRef.value.trim()
                        }))

                        emailRef.value = ""
                        passwordRef.value = ""
                    }}>
                        <legend className="text-center text-capitalize">Blockmeta</legend>
                        <Field
                            name="email"
                            label="邮箱"
                            placeholder="请输入邮箱"
                            _ref={node => emailRef = node}
                            onBlur={(e) => {
                                if (!EMAIL_PATTERN.test(e.target.value.trim())) {
                                    emailRef.classList.add("error")
                                    n1.classList.remove("hide")
                                } else {
                                    emailRef.classList.remove("error")
                                    n1.classList.add("hide")
                                }
                            }}
                        >
                            <p className="text-danger hide" style={{ marginTop: 5 }} ref={node => n1 = node}>邮箱格式不对</p>
                        </Field>
                        <Field
                            name="password"
                            label="密码"
                            type="password"
                            placeholder="请输入密码"
                            _ref={node => passwordRef = node}
                            onBlur={(e) => {
                                if (!PASSWORD_PATTERN.test(e.target.value.trim())) {
                                    passwordRef.classList.add("error")
                                    n2.classList.remove("hide")
                                } else {
                                    passwordRef.classList.remove("error")
                                    n2.classList.add("hide")
                                }
                            }}
                        >
                            <p className="text-danger hide" style={{ marginTop: 5 }} ref={node => n2 = node}>密码格式不对</p>
                            <p className="help-block" style={{ fontSize: 12 }}>密码由数字和英文字母组成，最少8个字符</p>
                        </Field>
                        <div className="checkbox">
                            <label>
                                <input type="checkbox" checked={true} disabled={true} />
                                <span>我已经阅读<a href="https://blockmeta.com/docs/#terms" target="_blank">"用户协议"</a></span>
                            </label>
                        </div>
                        <button className="btn btn-success btn-lg btn-block text-capitalize" type="submit">注册</button>
                    </form>
                )}
            </div>
        </div>
    )
})
