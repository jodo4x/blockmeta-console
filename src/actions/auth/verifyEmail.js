import { replace } from "react-router-redux"
import {
    VERIFY_EMAIL_REQUEST,
    VERIFY_EMAIL_SUCCESS,
    VERIFY_EMAIL_FAILURE
} from "../../constants/actionTypes"
import { put } from "../../middleware/api"

export default (data) => {
    return dispatch => {
        dispatch(verifyEmailRequest)

        put("/auth/activate", data)
        .then(json => {
            dispatch(verifyEmailSuccess({
                status: "success",
                message: "恭喜，验证成功，你现在可以使用该帐户了"
            }))
            dispatch(replace("/login"))
        }, error => {
            const messages = {
                "1004": "对不起，验证失败",
                "1005": "对不起，该链接已经过期，请重新再试一次",
                "1014": "对不起，你已经尝试太多次了"
            }
            dispatch(verifyEmailFailure({
                status: "warning",
                message: messages[error.code]
            }))
        })
    }
}

const verifyEmailRequest = {
    type: VERIFY_EMAIL_REQUEST
}

function verifyEmailSuccess(message) {
    return {
        type: VERIFY_EMAIL_SUCCESS,
        payload: {
            message
        }
    }
}

function verifyEmailFailure(message) {
    return {
        type: VERIFY_EMAIL_FAILURE,
        payload: {
            message
        }
    }
}
