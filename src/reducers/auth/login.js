import {
    LOGIN_SUCCESS,
    LOGIN_FAILURE
} from "../../constants/actionTypes"
import { assign } from "../../utils"

export default (state, { type, payload }) => {
    switch(type) {
        case LOGIN_SUCCESS:
            return assign({}, state, {
                isAuthenticated: true,
                message: payload.message
            })
        case LOGIN_FAILURE:
            return assign({}, state, {
                message: payload.message
            })
        default:
            return state
    }
}
