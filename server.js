var express = require("express")
var path = require("path")
var app = express()

var publicDir = __dirname + "/build"

app.get("/", function(req, res) {
    res.sendFile(path.join(publicDir, "/index.html"))
})

app.use(express.static(publicDir))

app.listen(3322, function() {
    console.log("now serving at port: 3322")
})
