import {
    UPDATE_MOBILE_SUCCESS,
    UPDATE_MOBILE_FAILURE
} from "../../constants/actionTypes"
import { assign } from "../../utils"

export default (state, { type, payload }) => {
    switch(type) {
        case UPDATE_MOBILE_SUCCESS:
        case UPDATE_MOBILE_FAILURE:
            return assign({}, state, {
                message: payload.message
            })
        default:
            return state
    }
}
