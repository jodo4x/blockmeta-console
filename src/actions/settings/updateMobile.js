import { put } from "../../middleware/api"
import {
    UPDATE_MOBILE_REQUEST,
    UPDATE_MOBILE_SUCCESS,
    UPDATE_MOBILE_FAILURE
} from "../../constants/actionTypes"

export default (data) => {
    return dispatch => {
        dispatch(updateMobileRequest)

        put("/users/me/reset-mobile", data)
        .then(json =>
            dispatch(updateMobileSuccess({
                status: "success",
                message: "你的手机号码已经更改成功"
            }))
        , error => {
            const messageMap = {
                "1005": "对不起，短信验证码不正确或者已过期",
                "1007": "哎呀，该手机号码已经被绑定"
            }

            dispatch(updateMobileFailure({
                status: "warning",
                message: messageMap[error.code] || "哎呀，出现了一些其它错误，请再试一次"
            }))
        })
    }
}

const updateMobileRequest = {
    type: UPDATE_MOBILE_REQUEST
}

function updateMobileSuccess(message) {
    return {
        type: UPDATE_MOBILE_SUCCESS,
        payload: {
            message
        }
    }
}

function updateMobileFailure(message) {
    return {
        type: UPDATE_MOBILE_FAILURE,
        payload: {
            message
        }
    }
}
