import { CHANGE_CONTROLLED_COMPONENT } from "../constants/actionTypes"
import { assign } from "../utils"

export default (state, { type, payload }) => {
    if (type === CHANGE_CONTROLLED_COMPONENT) {

        // "foo.bar"
        // "foo"
        const props = payload.statePropName.split(".")

        if (props.length === 1) {
            return assign({}, state, {
                [props[0]]: assign({}, state[props[0]], {
                    [payload.target.name]: payload.target.value
                })
            })
        }

        if (props.length === 2) {
            return assign({}, state, {
                [props[0]]: assign({}, state[props[0]], {
                    [props[1]]: assign({}, state[props[0]][props[1]], {
                        [payload.target.name]: payload.target.value
                    })
                })
            })
        }

        // TODO `props.length === 3` ?
    }
}
