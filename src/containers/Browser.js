import React, { PropTypes, Component } from "react"
import { connect } from "react-redux"
import Navbar from "../components/browser/Navbar"
import Menubar from "../components/browser/Menubar"
import getProfile from "../actions/settings/getProfile"
import queryNotifications from "../actions/notifications/queryNotifications"
import toggleNavbar from "../actions/toggleNavbar"
import logout from "../actions/auth/logout"

export default connect(
    state => ({
        isNavbarCollapsed: state.isNavbarCollapsed,
        profile: state.profile,
        notifications: state.notifications
    })
)(class Browser extends Component {
    componentDidMount() {
        this.props.dispatch(getProfile())
        this.props.dispatch(queryNotifications())
    }

    render() {
        let { location, children, dispatch } = this.props

        return (
            <div className="browser-container">
                <Navbar location={location} />
                <Menubar
                    {...this.props}
                    handleToggleNavbar={() => dispatch(toggleNavbar)}
                    handleLogout={() => dispatch(logout())}
                />
                <div className="browser-content clearfix">
                    {children}
                </div>
            </div>
        )
    }
})
