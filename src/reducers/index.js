import { cookie, assign } from "../utils"
import { TOGGLE_NAVBAR, RESET_MESSAGE, SHOW_CAPTCHA } from "../constants/actionTypes"
import changeControlledComponent from "./changeControlledComponent"
import login from "./auth/login"
import logout from "./auth/logout"
import getRecoveryPasswordEmail from "./auth/getRecoveryPasswordEmail"
import resetPassword from "./auth/resetPassword"
import signup from "./auth/signup"
import verifyEmail from "./auth/verifyEmail"
import getProfile from "./settings/getProfile"
import updateProfile from "./settings/updateProfile"
import getSMSCode from "./settings/getSMSCode"
import updateMobile from "./settings/updateMobile"
import updatePassword from "./settings/updatePassword"
import uploadAvatar from "./settings/uploadAvatar"
import notification from "./notifications"
import assets from "./assets"
import mining from "./mining"

const initState = {
    isFetching: false,
    isAuthenticated: !!cookie.get("_at_"),

    isNavbarCollapsed: false,

    miningInfo: {
        current_difficulty: 0, // NOTE: required!
        current_hash_rate: 0   // NOTE: required!
    },

    errDetails: {},

    spinners: {
    }

    // FIXME
    // profile: {
        // NOTE: can NOT BE removed
    //     avatar: "http://maoxiang-static-head.b0.upaiyun.com/o4jnf5tz6z2vxl8g.jpg"
    // }
}

const reducerMap = {
    "CHANGE_CONTROLLED_COMPONENT": changeControlledComponent,
    "LOGIN": login,
    "LOGOUT": logout,
    "SIGNUP": signup,
    "VERIFY_EMAIL": verifyEmail,
    "GET_RECOVERY_PASSWORD_EMAIL": getRecoveryPasswordEmail,
    "RESET_PASSWORD": resetPassword,

    "GET_PROFILE": getProfile,
    "UPDATE_PROFILE": updateProfile,
    "GET_SMSCODE": getSMSCode,
    "UPDATE_MOBILE": updateMobile,
    "UPDATE_PASSWORD": updatePassword,
    "UPLOAD_AVATAR": uploadAvatar,

    "NOTIFICATION": notification,

    "CLICK_ADD_ASSET": assets,
    "LOAD_MY_ASSETS": assets,
    "CREATE_ASSET": assets,
    "DELETE_ASSET": assets,
    "ADD_ASSET_MONITOR": assets,
    "GET_MODAL_ASSET": assets,
    "QUERY_ASSETS": assets,
    "GET_ASSET": assets,
    "GET_ASSETS_STATE": assets,

    "GET_MINING_INFO": mining,
    "CALCULATE_MINING": mining,
    "GET_DISTRIBUTION": mining,
    "GET_HASH_RATE": mining,
    "GET_MARKET": mining
}

export default (state = initState, action) => {
    const type = action.type

    // RESET GLOBAL MESSAGE
    if (type === RESET_MESSAGE) {
        return assign({}, state, {
            message: null
        })
    }

    // TODO
    // SHOW CAPTCHA
    if (type === SHOW_CAPTCHA) {
        return assign({}, state, {
            captcha: action.payload.captcha
        })
    }

    if (type === TOGGLE_NAVBAR) {
        return assign({}, state, {
            isNavbarCollapsed: !state.isNavbarCollapsed
        })
    }

    // 除了 算力 之外的所有请求
    if (!/GET_(DISTRIBUTION|MINING_INFO|HASH_RATE)/.test(type)) {
        if (/REQUEST/.test(type)) {
            state = assign({}, state, {
                isFetching: true,
                message: null
            })
        }
    }

    if (/SUCCESS|FAILURE/.test(type)) {
        state = assign({}, state, {
            isFetching: false
        })
    }

    for (let prop in reducerMap) {
        if (type.indexOf(prop) > -1) {
            return reducerMap[prop](state, action)
        }
    }

    return state
}
