import { get } from "../../middleware/api"
import { 
    QUERY_NOTIFICATIONS_REQUEST,
    QUERY_NOTIFICATIONS_SUCCESS,
    QUERY_NOTIFICATIONS_FAILURE
} from "../../constants/actionTypes"

export default (page = 1) => {
    return dispatch => {
        dispatch(queryNotificationsRequest)
        get(`/messages?page=${page}&per_page=50`)
        .then(json => {
            dispatch(queryNotificationsSuccess(json))
        }, error =>
            dispatch(queryNotificationsFailure({
                status: "warning",
                message: "哎呀，获取通知失败，请刷新试试"
            }))
        )
    }
}

const queryNotificationsRequest = {
    type: QUERY_NOTIFICATIONS_REQUEST
}

function queryNotificationsSuccess(json) {
    return {
        type: QUERY_NOTIFICATIONS_SUCCESS,
        payload: {
            data: json.data
        }
    }
}

function queryNotificationsFailure(message) {
    return {
        type: QUERY_NOTIFICATIONS_FAILURE,
        payload: {
            message
        }
    }
}
