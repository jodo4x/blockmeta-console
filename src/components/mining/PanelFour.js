import React from "react"
import ScrollArea from "react-scrollbar"

export default class PanelFour extends React.Component {
    render() {
        let data = this.props.calculateResult||{}

        return (
            <div className="panel panel-default panel4">
                <div className="panel-heading">
                    <h2 className="panel-title"><span>计算</span>结果</h2>
                </div>

                <div className="panel-body">
                    <div style={{ height: 495 }}>
                        <table className="table text-center result-t1">
                            <tbody>
                                <tr>
                                    <td>
                                        <div className="d1">预计总收益</div>
                                        <div className="d2">{data.earnings && (data.earnings >= 10000000 ? data.earnings.toExponential(3) : data.earnings.toFixed(0).toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,')) || "--"}</div>
                                    </td>
                                    <td>
                                        <div className="d1">实际收益</div>
                                        <div className="d2">{data.net_profits && (data.net_profits >= 10000000 ? data.net_profits.toExponential(3) : data.net_profits.toFixed(0).toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,')) || "--"}</div>
                                    </td>
                                    <td>
                                        <div className="d1">回本时间</div>
                                        <div className="d2">{data.earn_back_day && data.earn_back_day + "天" || "--"}</div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <ScrollArea
                            speed={0.4}
                            className="foobar"
                            style={{ width: "100%", height: 340 }}
                            contentClassName="content"
                            horizontal={false}
                        >
                            <table className="table result-t2">
                                <thead>
                                    <tr>
                                        <th className="col1">#</th>
                                        <th className="col2">时间</th>
                                        <th className="col3">跨度</th>
                                        <th className="col4">预计收益额</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {data && data.cycle && data.cycle.map((item, index) => {
                                        return (
                                            <tr key={index}>
                                                <td className="col1">{index+1}</td>
                                                <td className="col2">{item.start.replace(/-/g, "/")}-{item.end.replace(/-/g,"/")}</td>
                                                <td className="col3">{item.days}天</td>
                                                <td className="col4">{item.earnings > 10000000 ? item.earnings.toExponential(3) : item.earnings.toFixed(0).toString().replace(/(\d)(?=(?:\d{3})+$)/g, '$1,')}</td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </ScrollArea>
                    </div>
                </div>
            </div>
        )
    }
}
