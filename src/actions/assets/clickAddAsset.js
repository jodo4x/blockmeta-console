import {
    CLICK_ADD_ASSET
} from "../../constants/actionTypes"

function clickAddAsset(isShown) {
    return {
        type: CLICK_ADD_ASSET,
        payload: {
            isShown
        }
    }
}

export default clickAddAsset
