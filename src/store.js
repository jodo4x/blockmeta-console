import { createStore, applyMiddleware } from "redux"
import thunkMiddleware from "redux-thunk"
import { hashHistory } from "react-router"
import { routerMiddleware } from "react-router-redux"
import app from "./reducers"

const store = createStore(
    app,
    applyMiddleware(
        thunkMiddleware,
        routerMiddleware(hashHistory)
    )
)

export default store
