import { get } from "../../middleware/api"

export default function getAssetsState() {
    return dispatch => {
        get("/assets/stat")
        .then(json => {
            dispatch(getAssetsStateSuccess(json))
        }, err => {
            dispatch(getAssetsStateFailure({
                status: "warning",
                message: "哎呀，获取资源出错，请再试一次"
            }))
        })
    }
}

function getAssetsStateSuccess(json) {
    return {
        type: "GET_ASSETS_STATE_SUCCESS",
        payload: {
            statistics: json
        }
    }
}

function getAssetsStateFailure(message) {
    return {
        type: "GET_ASSETS_STATE_FAILURE",
        payload: {
            message
        }
    }
}
