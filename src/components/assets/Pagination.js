import React from "react"

export default ({ assets, handlePagerPrev, handlePagerNext, handlePager }) =>
    assets &&
    assets.pages > 1 &&
    (<div className="text-center">
        <ul className="pagination">
            {assets.has_prev ? (
                <li>
                    <a href="javascript:;" onClick={handlePagerPrev}>&lt;</a>
                </li>
            ) : (
                <li>
                    <a href="javascript:;" className="disabled-link">&lt;</a>
                </li>
            )}

            {[...(new Array(assets.pages))].map((pager, index) => {
                return (
                    <li
                        key={index}
                        className={assets.page === (index+1) ? "active" : ""}
                    >
                        {assets.page === (index+1) ? (
                            <a href="javascript:;" className="disabled-link">{index+1}</a>
                        ) : (
                            <a href="javascript:;" onClick={() => handlePager(index+1)}>{index+1}</a>
                        )}
                    </li>
                )
            })}

            {assets.has_next ? (
                <li>
                    <a href="javascript:;" onClick={handlePagerNext}>&gt;</a>
                </li>
            ) : (
                <li>
                    <a href="javascript:;" className="disabled-link">&gt;</a>
                </li>
            )}

        </ul>
    </div>) ||
    null
