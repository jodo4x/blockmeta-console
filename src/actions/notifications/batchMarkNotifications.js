import { post } from "../../middleware/api"
import {
    BATCH_MARK_NOTIFICATIONS_REQUEST,
    BATCH_MARK_NOTIFICATIONS_SUCCESS,
    BATCH_MARK_NOTIFICATIONS_FAILURE
} from "../../constants/actionTypes"
import queryNotifications from "./queryNotifications"

export default (data) => {
    return dispatch => {
        dispatch(batchMarkNotificationsRequest)

        post("/messages/batch", {
            update: (function() {
                let ret = {}
                data.forEach(item => ret[item] = {
                    // FIXME 这里只支持标记为`已读`
                    is_read: true
                })
                return ret
            }())
        })
        .then(json => {
            dispatch(batchMarkNotificationsSuccess({
                status: "success",
                message: "更新成功"
            }))

            // 重新获取列表
            dispatch(queryNotifications())
        }, error =>
            dispatch(batchMarkNotificationsFailure({
                status: "warning",
                message: "哎呀，出现了一些错误，请再试一次"
            }))
        )
    }
}

const batchMarkNotificationsRequest = {
    type: BATCH_MARK_NOTIFICATIONS_REQUEST
}

function batchMarkNotificationsSuccess(message) {
    return {
        type: BATCH_MARK_NOTIFICATIONS_SUCCESS,
        payload: {
            message
        }
    }
}

function batchMarkNotificationsFailure(message) {
    return {
        type: BATCH_MARK_NOTIFICATIONS_FAILURE,
        payload: {
            message
        }
    }
}
