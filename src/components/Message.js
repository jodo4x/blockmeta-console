import React, { PropTypes } from "react"
import { connect } from "react-redux"
import ReactCSSTransitionGroup from "react-addons-css-transition-group"
import resetMessage from "../actions/resetMessage"

const Message = ({ message, dispatch }) => {
    if (message) {
        let timer = setTimeout(() => {
            dispatch(resetMessage)
            clearTimeout(timer)
        }, 3000)
    }

    return (
        <ReactCSSTransitionGroup
            transitionName="alert"
            transitionEnterTimeout={300}
            transitionLeaveTimeout={250}
        >
        {message ? (
            <div className={"g-alert alert alert-" + message.status}>
                <div className="container-fluid">
                    <div className="text-center">
                        <button
                            className="close"
                            type="button"
                            onClick={e => dispatch(resetMessage)}
                        >
                            <span>&times;</span>
                        </button>
                        {message.message}
                    </div>
                </div>
            </div>
        ) : null}
        </ReactCSSTransitionGroup>
    )
}

Message.propTypes = {
    message: PropTypes.shape({
        message: PropTypes.string.isRequired
    })
}

export default connect()(Message)
