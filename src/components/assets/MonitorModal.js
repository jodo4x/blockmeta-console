import React from "react"
import Modal from "../Modal"
import Switch from "react-toggle-switch"

export default class MonitorModal extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            monitor: false,
            in_notify: false,
            out_notify: false
        }
        this.getNotifies = this.getNotifies.bind(this)
    }

    getNotifies() {
        return {
            monitor: this.state.monitor,
            in_notify: this.state.in_notify,
            out_notify: this.state.out_notify
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps !== this.props) {
            // 异步渲染
            const props = nextProps

            this.setState({
                monitor: props.asset && props.asset.monitor,
                in_notify: props.asset && props.asset.condition && props.asset.condition.in_notify,
                out_notify: props.asset && props.asset.condition && props.asset.condition.out_notify
            })
        }
    }

    render() {
        let props = this.props

        return (
            <Modal title="添加监控" isMonitorUpdated={props.isMonitorUpdated} ref={props.modal} handleSubmit={props.handleSubmit}>
                <div className="row">
                    <div className="col-sm-12">
                        <form>
                            <div className="form-group n1">
                                <label className="control-label" htmlFor="field__addr">所监控地址</label>
                                <p className="form-control-static"><code style={{ fontSize: 18 }}>{props.asset.address}</code></p>
                            </div>

                            <hr />
                            <div className="form-group" style={{ marginBottom: 0 }}>
                                <label htmlFor="field__monitor">
                                    <span style={{ marginRight: 5 }}>开启监控</span>
                                    <Switch
                                        on={this.state.monitor}
                                        onClick={() => {
                                            this.setState({
                                                monitor: !this.state.monitor
                                                // in_notify: !this.state.monitor ? false : this.state.in_notify,
                                                // out_notify: !this.state.monitor ? false : this.state.out_notify
                                            }, () => {
                                                props.handleChangeControlledComponent2({
                                                    target: {
                                                        type: "checkbox",
                                                        id: "field__monitor",
                                                        checked: this.state.monitor
                                                    }
                                                })
                                            })
                                        }}
                                    />
                                </label>
                            </div>
                            <hr />

                            <div style={{ marginBottom: 10 }}><strong>监控条件</strong>:</div>
                            <div className="form-group n2">
                                <div className="row">
                                    <div className="col-sm-5">
                                        <label htmlFor="field__in_notify">
                                            <span style={{ marginRight: 8 }}>转入提醒</span>
                                            <Switch
                                                on={this.state.in_notify}
                                                enabled={this.state.monitor}
                                                onClick={() => {
                                                    this.setState({ in_notify: !this.state.in_notify })
                                                }}
                                            />
                                        </label>
                                    </div>
                                    <div className="col-sm-7">
                                        <label htmlFor="field__in_min">
                                            <span>转入超过</span>
                                            <input
                                                className="form-control"
                                                id="field__in_min"
                                                type="text"
                                                value={props.asset && props.asset.condition && props.asset.condition.in_min !== 0 ? props.asset.condition.in_min : ""}
                                                ref={props.inMin}
                                                disabled={!this.state.monitor}
                                                onChange={e => props.handleChangeControlledComponent(e)}
                                            />
                                            <span>BTC</span>
                                            <span>提醒</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group n3">
                                <div className="row">
                                    <div className="col-sm-5">
                                        <label htmlFor="field__out_notify">
                                            <span style={{ marginRight: 8 }}>转出提醒</span>
                                            <Switch
                                                on={this.state.out_notify}
                                                enabled={this.state.monitor}
                                                onClick={() => {
                                                    this.setState({ out_notify: !this.state.out_notify })
                                                }}
                                            />
                                        </label>
                                    </div>
                                    <div className="col-sm-7">
                                        <label htmlFor="field__out_min">
                                            <span>转出超过</span>
                                            <input
                                                className="form-control"
                                                id="field__out_min"
                                                type="text"
                                                value={props.asset && props.asset.condition && props.asset.condition.out_min !== 0 ? props.asset.condition.out_min : ""}
                                                ref={props.outMin}
                                                disabled={!this.state.monitor}
                                                onChange={e => props.handleChangeControlledComponent(e)}
                                            />
                                            <span>BTC</span>
                                            <span>提醒</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div className="form-group n4">
                                <label className="control-label" htmlFor="field__email">提醒邮箱</label>
                                <input
                                    className="form-control"
                                    id="field__email"
                                    type="text"
                                    value={props.asset && props.asset.condition ? props.asset.condition.email : ""}
                                    ref={props.notifyEmail}
                                    disabled={!this.state.monitor}
                                    onChange={e => props.handleChangeControlledComponent(e)}
                                />
                            </div>
                        </form>
                    </div>
                </div>
            </Modal>
        )
    }
}
