import { get } from "../../middleware/api"

export default (data) => {
    return dispatch => {
        dispatch(getHashRateRequest)
        get(`/mining/hash_rate?timespan=${data.timespan}`)
        .then(json => {
            dispatch(getHashRateSuccess(json))
        }, err => {
            dispatch(getHashRateFailure({
                status: "warning",
                message: "哎呀，出现了一些错误，请再试一次。"
            }))
        })
    }
}

const getHashRateRequest = {
    type: "GET_HASH_RATE_REQUEST",
    payload: {
        isFetching: true
    }
}

function getHashRateSuccess(json) {
    return {
        type: "GET_HASH_RATE_SUCCESS",
        payload: {
            hashRate: json.data,
            isFetching: false
        }
    }
}

function getHashRateFailure(message) {
    return {
        type: "GET_HASH_RATE_FAILURE",
        payload: {
            message,
            isFetching: false
        }
    }
}
