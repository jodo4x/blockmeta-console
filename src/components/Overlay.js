import React, { PropTypes } from "react"

const styles = {
    backgroundColor: "rgba(0, 0, 0, .35)",
    position: "fixed",
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    width: "100%",
    height: "100%",
    zIndex: 9999 
}

const Overlay = ({ isFetching }) =>
    isFetching ? (
        <div style={styles}>
            <div className="sk-fading-circle">
                {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11].map((num, index) => {
                    return <div className={"sk-circle" + num + " sk-circle"} key={index}></div>
                })}
            </div>
        </div>
    ) : null

Overlay.propTypes = {
    isFetching: PropTypes.bool.isRequired
}

export default Overlay
