import React from "react"
import addressTypes from "../../constants/addressTypes"
import Modal from "../Modal"

export default (props) => {
    let modal
    return (
        <tr>
            <td style={{ width: "35%" }}><code>{props.asset.address}</code></td>
            <td style={{ width: "20%" }}>
                {addressTypes.filter((obj) => {
                    return obj.key === props.asset.type
                })[0].name}
            </td>
            <td style={{ width: "20%" }}>{props.asset.mark}</td>
            <td style={{ width: "25%" }} className="text-right">
                <button
                    className="btn btn-link"
                    type="button"
                    onClick={() => {
                        props.handleAddMonitor(props.asset.id)
                        props.handleGetAsset(props.asset.id)
                    }}
                >
                    {props.asset.monitor && <img src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAcCAYAAAAAwr0iAAAABGdBTUEAALGPC/xhBQAAAyhJREFUSA3tV29IU1EU/73tzTm19FlqS8spZIRa0YeCwhL7kCEGJRIkFERYIH2ISBKJiqAPBVEQFCQEhhkURNDfL2VCmRhJWqTV1Gym88+cm24+9697H+7xNva2p8196o6xc88593d+u+fcd+5jfGTMeGYx6ZoGQz6xGD74wGmSkKiOB8t7Xaj9cRdfpn/B5fPEIj40jBoFSdm4lncM7IyHR+/MENLjUlDMFRJuviUlQXe5ZbJbiEljs/5Nz0tYjZo1ZUsa3A/+h59A+9R3IeEqv9LjW9p/7o9Df6WxRAJSh1jK/wlEfQfarD2o/9mIYd6iKJNRJdBBKrve2IhW61fwXndsCdAH2TljE1xeDy7mHoJBlx59Ap02I071NuCNpSsA3OgYJtt+DzaPA2cMB1CSuinAHm7ChjNKbabZcVzoa8aYawrttl6c4PfisL5EyHUdyTnV12SVYV/aNumyiLIiArRZXep/IAQpX7kVXdMDuG16gQGnmXxHYeLHcUS/G1X64ogBgx0iEiDNEtcHn6Cb5LiE24i6nEqYeatA6OXEJwGvIn07qjP3BGMrmkck0GxuxbPxj6C9otZQIYBmaFNwdd1RNAy9gopR4XhmKRjG31UUxRWdwhJ4b/2GW6bn4NhEnCeVvZxNEBfq1HE4ubZcnC9WkH0O0Nxe7n8I2p3PGiqRo8tYbIyw60QCKskW2t1OkuNmWNx2VGeVoojLDwuyUKM0lpiCTjs943cELHqk+kiFl67YgqpVu0LiPzK/w4epHtJavSHt4ZQ9DpNoZpepddjJFeCzvQ9G54hgUJPCKkrJx+ns/UKRid7zAr3GNY20wDxnRZomecG3qASVFpuTc0FjM/RSSnFnPXMBceJJkckN6nuw+wr0Wg431ldjHkLOPaTejy+mwK8I6S2jVJFLlValkbEqU4tFqMw9+l6LIkB3ix4a+hD61yGmIBjI4rLj8WgbnN7A2qB+blL5NrcDg74x3Pz9NHipMC9IzEZxamFIm1QpS4B2v8bh17IvK3EMK5C4P/JWiifKO5I3KCIgngJxpUQYJcdsjtxsFvrKJn31ksCFFP8CHaYldrscLzYAAAAASUVORK5CYII="} alt=""  style={{ width: 16, height: 14, marginRight: 5, position: "relative", top: -2 }} />}
                    {!props.asset.monitor ? "添加监控" : "监控"}
                </button>
                <button
                    className="btn btn-link btn-del"
                    type="button"
                    onClick={() => {
                        modal.open()
                    }}
                >
                    <img src={"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAeCAYAAAA/xX6fAAAABGdBTUEAALGPC/xhBQAAAnxJREFUSA3tVltPE1EQ/k63FzUQUDFUoFRjQJr4IF4fxKhpYjAmGuOTf9DESGLAh/KAlwchqRI0FLFaKIjViKGt7WolobvrGWRP95xtpdE18aGTNDu3M9/O7JzOMIsTmqClbA4zzxdQNUzJOxQM4PLISfQePiTpGwmsGcDMcg73x5+gJ9yFcPcBKdb7D+vQ9Qru3I6jt2d3UKZ/q1jl8ncpiFPYKJTwcHIGA8f6cOv6RQQCfqcZlR+buPfgMTbyJdwYvYD2tn2SXQgM2N/ZDjb5KGnNvkwLvcqYpoXY8ShuXhtxgdm+BDo2/hSUrc/HI9clhvilU2CVyqZV0htn6GMMXQc7eCBf3TC2cmurisJXHY1agl6jkzJs5hvaQb14/v61vUBQYgjAufl3WF75qJgBwzBcZTJNE/RzEpXSUK4M2VOLWaQza8JVAM7OpbH4dlUYiKEAd8empAOkT0wlMZ1MESvoBT8/kXgmZJt5lcrg9ZsVW4TocU3TXI1BWXz+UkBZaao8vyrVqiGCEFMs6dtXQ1JygeJqmsgLNU713JE13p3UqU6ijnXpuA9reCVqp3cFrLl6w7UAvamjI0qrpI5ieMO2SupNHR1R/r+S0p+0qSx2pDOU8USrSL3x5EhumxXTQjWQ7PdrfAUcxpH+sGQ+MzyEvXuCkm5osN+10UkOO4IAtGCBKVOB5HOnY65zJ2JHXbpoJIxoxKWGPGdQG080bixeFq9JLb3IMNLXjfmFJQwORH7tlh5g54slrOXWcfXKWZGH2Npot5xITCO7+kkY/5ahT0LlH42fFzutALSDF4pl0I7JP6it+rMn7+xQKIjOjjbpvAtQsv4D4ScpjOnVV5OneAAAAABJRU5ErkJggg=="} alt="" style={{ width: 14, height: 15 }} />
                </button>
                <Modal
                    title="确认删除资源"
                    ref={node => modal = node}
                    isAssetDeleted={props.isAssetDeleted}
                    handleSubmit={() => props.handleDeleteAsset(props.asset.id)}
                >
                    <div>资源一旦删除，不可恢复。</div>
                </Modal>
            </td>
        </tr>
    )
}
