import { CLICK_TAB } from "../../constants/actionTypes"

const clickTab = {
    type: CLICK_TAB
}

export default clickTab
