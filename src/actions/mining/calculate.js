import { put } from "../../middleware/api"

export default function calculateMining(data) {
    return dispatch => {
        dispatch(calculateMiningRequest)

        put("/mining/calculator", data)
        .then(json => {
            dispatch(calculateMiningSuccess(json))
        }, err => {
            console.warn(err)

            const messages = {
                "1004": "请检查你的参数是否正确。"
            }

            dispatch(calculateMiningFailure({
                errDetails: getErrObj(err),
                message: {
                    status: "warning",
                    message: messages[err.code] || "哎呀，出现了一些错误，请再试一次"
                }
            }))
        })
    }
}

const calculateMiningRequest = {
    type: "CALCULATE_MINING_REQUEST",
    payload: {
        errDetails: {}
    }
}

function calculateMiningSuccess(json) {
    return {
        type: "CALCULATE_MINING_SUCCESS",
        payload: {
            calculateResult: json
        }
    }
}

function calculateMiningFailure(data) {
    return {
        type: "CALCULATE_MINING_FAILURE",
        payload: {
            message: data.message,
            errDetails: data.errDetails
        }
    }
}

function getErrObj(err) {
    let ret = { }
    for (let p in err.details) {
        switch(p) {
            case "current_difficulty":
                ret.inpt1 = true; break
            case "difficulty_increase":
                ret.inpt2 = true; break
            case "difficulty_increase_cycle":
                ret.inpt3 = true; break
            case "mm_price":
                ret.inpt4 = true; break
            case "mm_power":
                ret.inpt5 = true; break
            case "electricity_costs":
                ret.inpt6 = true; break
            case "mm_hash_rate":
                ret.inpt7 = true; break
            case "conversion_rate":
                ret.inpt8 = true; break
            case "mine_pool_fee":
                ret.inpt9 = true; break
            case "mm_extras":
                ret.inpt10 = true; break
            case "start_time":
                ret.start_time = true; break
            case "end_time":
                ret.end_time = true; break
        }
    }

    return ret
}
