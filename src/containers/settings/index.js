import React, { PropTypes } from "react"
import { Link } from "react-router"

const Settings = ({ location, children }) => (
    <div className="settings-container">
        <ul className="nav nav-tabs text-capitalize">
            <li className={location.pathname.indexOf("profile") > -1 ? "active": ""}>
                <Link to="/settings/profile">个人资料</Link>
            </li>
            <li className={location.pathname.indexOf("password") > -1 ? "active": ""}>
                <Link to="/settings/password">修改密码</Link>
            </li>
        </ul>
        <div className="tab-content">
            {children}
        </div>
    </div>
)

Settings.propTypes = {
    children: PropTypes.node.isRequired
}

export default Settings
