import { destroy } from "../../middleware/api"
import {
    DELETE_NOTIFICATION_REQUEST,
    DELETE_NOTIFICATION_SUCCESS,
    DELETE_NOTIFICATION_FAILURE
} from "../../constants/actionTypes"
import queryNotifications from "./queryNotifications"

export default (id) => {
    return dispatch => {
        dispatch(deleteNotificationRequest)

        destroy(`/messages/${id}`)
        .then(json => {
            dispatch(deleteNotificationSuccess({
                id: id,
                message: {
                    status: "success",
                    message: "删除成功"
                }
            }))
            dispatch(queryNotifications())
        }, error =>
            dispatch(deleteNotificationFailure({
                status: "warning",
                message: "哎呀，出现了一些错误，请再试一次"
            }))
        )
    }
}

const deleteNotificationRequest = {
    type: DELETE_NOTIFICATION_REQUEST
}

function deleteNotificationSuccess(data) {
    return {
        type: DELETE_NOTIFICATION_SUCCESS,
        payload: {
            id: data.id,
            message: data.message
        }
    }
}

function deleteNotificationFailure(message) {
    return {
        type: DELETE_NOTIFICATION_FAILURE,
        payload: {
            message
        }
    }
}
