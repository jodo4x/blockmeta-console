import { post } from "../../middleware/api"
import {
    BATCH_DELETE_NOTIFICATIONS_REQUEST,
    BATCH_DELETE_NOTIFICATIONS_SUCCESS,
    BATCH_DELETE_NOTIFICATIONS_FAILURE
} from "../../constants/actionTypes"
import queryNotifications from "./queryNotifications"

export default (data) => {
    return dispatch => {
        dispatch(batchDeleteNotificationsRequest)

        post("/messages/batch", {
            "delete": data
        })
        .then(json => {
            dispatch(batchDeleteNotificationsSuccess({
                status: "success",
                message: "删除成功"
            }))
            dispatch(queryNotifications())
        }, error =>
            dispatch(batchDeleteNotificationsFailure({
                status: "warning",
                message: "哎呀，出现了一些错误，请再试一次"
            }))
        )
    }
}

const batchDeleteNotificationsRequest = {
    type: BATCH_DELETE_NOTIFICATIONS_REQUEST
}

function batchDeleteNotificationsSuccess(message) {
    return {
        type: BATCH_DELETE_NOTIFICATIONS_SUCCESS,
        payload: {
            message
        }
    }
}

function batchDeleteNotificationsFailure(message) {
    return {
        type: BATCH_DELETE_NOTIFICATIONS_FAILURE,
        payload: {
            message
        }
    }
}
