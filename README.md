BlockMeta
=========

## 部署流程

**注意: 所有命令确保是在当前目录下操作的**

----

### 1. 需要重新构建

```
$ npm run build
$ npm run clear
$ npm run deploy
```

### 2. 不需要重新构建

```
$ npm run clear
$ npm run deploy
```

----

## 重启服务

```
$ npm run reload
```
