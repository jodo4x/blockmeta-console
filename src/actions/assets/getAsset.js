import { get } from "../../middleware/api"
import {
    GET_ASSET_REQUEST,
    GET_ASSET_SUCCESS,
    GET_ASSET_FAILURE
} from "../../constants/actionTypes"

export default (id) =>
    dispatch => {
        get(`/assets/${id}`)
        .then(json => {
            dispatch(getAssetSuccess(json.data))
        }, err => {
            dispatch(getAssetFailure({
                status: "warning",
                message: "哎呀，获取信息出错，请再试一次。"
            }))
        })
    }

const getAssetRequest = {
    type: GET_ASSET_REQUEST
}

function getAssetSuccess(data) {
    return {
        type: GET_ASSET_SUCCESS,
        payload: {
            asset: data
        }
    }
}

function getAssetFailure(message) {
    return {
        type: GET_ASSET_FAILURE,
        payload: {
            message
        }
    }
}
