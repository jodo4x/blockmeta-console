import { get, post, patch } from "../../middleware/api"
import {
    UPLOAD_AVATAR_REQUEST,
    UPLOAD_AVATAR_SUCCESS,
    UPLOAD_AVATAR_FAILURE,
} from "../../constants/actionTypes"

export default (file) => {
    return dispatch => {
        dispatch(uploadAvatarRequest)

        get("/users/me/reset-avatar")
        .then(json => {
            uploadFileToUpyun(dispatch, json, file)
        }, error =>
            dispatch(uploadAvatarFailure({
                status: "warning",
                message: "哎呀，出现了一些错误，请再试一次"
            }))
        )
    }
}

const uploadAvatarRequest = {
    type: UPLOAD_AVATAR_REQUEST
}

function uploadAvatarSuccess({ avatar, message }) {
    return {
        type: UPLOAD_AVATAR_SUCCESS,
        payload: {
            message,
            avatar
        }
    }
}

function uploadAvatarFailure(message) {
    return {
        type: UPLOAD_AVATAR_FAILURE,
        payload: {
            message
        }
    }
}

function uploadFileToUpyun(dispatch, json, file) {
    let bucket = json.url
    let formData = new FormData

    formData.append("file", file)
    formData.append("policy", json.policy)
    formData.append("signature", json.signature)

    post(`http://${json.server}`, formData)
    .then(json => {
        saveAvatar(dispatch, `${bucket + json.url}`/*avatar url*/)
    }, error => {
        dispatch(uploadAvatarFailure({
            status: "danger",
            message: "哎呀，在上传的过程中出现了一些错误，请再试一次"
        }))
    })
}

function saveAvatar(dispatch, avatar) {
    patch("/users/me", { avatar: avatar })
    .then(json =>
        dispatch(uploadAvatarSuccess({
            avatar: avatar,
            message: {
                status: "success",
                message: "你的头像已经保存成功"
            }
        }))
    , error => {
        dispatch(uploadAvatarFailure({
            status: "warning",
            message: "哎呀，在保存头像的过程中遇到一些错误，请重新再试一次"
        }))
    })
}
