import { post } from "../../middleware/api"
import {
    CREATE_ASSET_REQUEST,
    CREATE_ASSET_SUCCESS,
    CREATE_ASSET_FAILURE
} from "../../constants/actionTypes"
import loadMyAssets from "./loadMyAssets"

export default (data) =>
    dispatch => {
        dispatch(createAssetRequest)

        post("/assets", data)
        .then(json => {
            dispatch(createAssetSuccess({
                asset: json.data,
                message: {
                    status: "success",
                    message: "创建成功"
                }
            }))
            dispatch(loadMyAssets())
        }, err => {
            const messageMap = {
                "1004": "创建失败! 请检查" + function() {
                    const map = { address: " 地址 ", mark: " 备注 " }
                    const arr = Object.keys(err.details||{}).map((p) => {
                        return map[p]
                    })

                    return arr.length > 1 ? arr.join("、") : arr.join("")
                }() + "是否填写正确",
                "1017": "创建失败，配额不足。",
                "1007": "创建失败，资源已存在。"
            }

            dispatch(createAssetFailure({
                status: "warning",
                message: messageMap[err.code] || "哎呀，出现了一些错误，请再试一次"
            }))
        })
    }

const createAssetRequest = {
    type: CREATE_ASSET_REQUEST
}

function createAssetSuccess(data) {
    return {
        type: CREATE_ASSET_SUCCESS,
        payload: {
            asset: data.asset,
            message: data.message
        }
    }
}

function createAssetFailure(message) {
    return {
        type: CREATE_ASSET_FAILURE,
        payload: {
            message
        }
    }
}
