import React, { Component } from "react"
import Spinner from "../Spinner"

export default class Mobile extends Component {
    constructor(props) {
        super(props)
        this.state = {
            count: null
        }
    }

    render() {
        let { profile, handleChange, getSMSCode, updateMobile } = this.props
        let { count } = this.state
        let mobile, code

        return (
            <div className="form-group">
                <label className="control-label" htmlFor="field__mobile">手机绑定</label>
                <div className="clearfix">
                    <div className="pull-left">
                        <div className="input-group">
                            <input
                                className="form-control input-lg"
                                id="field__mobile"
                                type="text"
                                value={profile.mobile||""}
                                ref={node => mobile = node}
                                onChange={handleChange}
                            />
                            <div className="input-group-addon">
                                <button
                                    className="btn btn-link"
                                    type="button"
                                    disabled={count ? true : false}
                                    onClick={e => {
                                        let count = 60
                                        const timer = setInterval(() => {
                                            if (count === 0) {
                                                clearInterval(timer)
                                                return this.setState({
                                                    count: null
                                                })
                                            }

                                            this.setState({
                                                count: count--
                                            })
                                        }, 1000)

                                        getSMSCode({ mobile: mobile.value.trim() })
                                    }}
                                >
                                    <span>{count ? count + "秒后重发" : "发送验证码" }</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div className="pull-left" style={{ marginLeft: 20 }}>
                        <input
                            className="form-control input-lg"
                            id="field__smscode"
                            type="text"
                            style={{ width: 180 }}
                            placeholder="请输入验证码"
                            ref={node => code = node}
                            onBlur={e => {
                                updateMobile({
                                    new_mobile: mobile.value.trim(),
                                    code: code.value.trim()
                                })
                                code.value = ""
                            }}
                        />
                    </div>
                </div>
            </div>
        )
    }
}
