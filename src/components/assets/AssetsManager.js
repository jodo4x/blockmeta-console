import React from "react"
import Toolbar from "./Toolbar"
import AssetList from "./AssetList"
import Pagination from "./Pagination"

export default (props) =>
    <div className="panel panel-default p2">
        <div className="panel-heading">
            <h3 className="panel-title">我的资产</h3>
        </div>
        <div className="panel-body">
            <Toolbar
                {...props}
            />
            <AssetList
                {...props}
            />
            <Pagination
                {...props}
            />
        </div>
    </div>
