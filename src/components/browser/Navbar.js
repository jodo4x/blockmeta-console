import React, { PropTypes } from "react"
import { Link } from "react-router"
import ReactTooltip from "react-tooltip"

export default ({ location }) => (
    <div className="browser-navbar">
        <div className="hidden-collapse">
            <Link to="/assets" className="brand">BM</Link>
            <ul className="nav nav-pills nav-stacked text-capitalize">
                <li data-tip data-for="n4" className={location.pathname === "/assets" && "active"}>
                    <Link to="/assets"><i className="fa fa-braille"></i></Link>
                    <ReactTooltip id="n4">
                        <span>资产</span>
                    </ReactTooltip>
                </li>
                <li data-tip data-for="n7" className={location.pathname === "/mining" && "active"}>
                    <Link to="/mining"><i className="fa fa-braille"></i></Link>
                    <ReactTooltip id="n7">
                        <span>算力</span>
                    </ReactTooltip>
                </li>
                {/*
                <li data-tip data-for="n1" className={location.pathname === "/foo1" && "active"}>
                    <a href="javascript:;" className="disabled-link"><i className="fa fa-area-chart"></i></a>
                    <ReactTooltip id="n1">
                        <span>币数据</span>
                    </ReactTooltip>
                </li>
                <li data-tip data-for="n2" className={location.pathname === "/foo2" && "active"}>
                    <a href="javascript:;" className="disabled-link"><i className="fa fa-money"></i></a>
                    <ReactTooltip id="n2">
                        <span>交易记录</span>
                    </ReactTooltip>
                </li>
                <li data-tip data-for="n3" className={location.pathname === "/foo3" && "active"}>
                    <a href="javascript:;" className="disabled-link"><i className="fa fa-line-chart"></i></a>
                    <ReactTooltip id="n3">
                        <span>创业数据</span>
                    </ReactTooltip>
                </li>
                <li data-tip data-for="n5" className={location.pathname === "/foo4" && "active"}>
                    <a href="javascript:;" className="disabled-link"><i className="fa fa-desktop"></i></a>
                    <ReactTooltip id="n5">
                        <span>地址监控</span>
                    </ReactTooltip>
                </li>
                <li data-tip data-for="n6" className={location.pathname === "/foo5" && "active"}>
                    <a href="javascript:;" className="disabled-link"><i className="fa fa-pie-chart"></i></a>
                    <ReactTooltip id="n6">
                        <span>交易统计</span>
                    </ReactTooltip>
                </li>
                */}
            </ul>
        </div>
        <div className="hidden-expand">
            <Link to="/assets" className="brand">BlockMeta</Link>
            <ul className="nav nav-pills nav-stacked text-capitalize">
                <li className={location.pathname === "/assets" && "active"}><Link to="/assets"><i className="fa fa-braille"></i>资产<i className="fa fa-chevron-right arrow"></i></Link></li>
                <li className={location.pathname === "/mining" && "active"}><Link to="/mining"><i className="fa fa-braille"></i>算力<i className="fa fa-chevron-right arrow"></i></Link></li>
                {/*
                <li className={location.pathname === "/foo1" && "active"}><a href="javascript:;" className="disabled-link"><i className="fa fa-area-chart"></i>币数据<i className="fa fa-chevron-right arrow"></i></a></li>
                <li className={location.pathname === "/foo2" && "active"}><a href="javascript:;" className="disabled-link"><i className="fa fa-money"></i>交易<i className="fa fa-chevron-right arrow"></i></a></li>
                <li className={location.pathname === "/foo3" && "active"}><a href="javascript:;" className="disabled-link"><i className="fa fa-line-chart"></i>创业数据<i className="fa fa-chevron-right arrow"></i></a></li>
                <li className={location.pathname === "/foo4" && "active"}><a href="javascript:;" className="disabled-link"><i className="fa fa-desktop"></i>地址监控<i className="fa fa-chevron-right arrow"></i></a></li>
                <li className={location.pathname === "/foo5" && "active"}><a href="javascript:;" className="disabled-link"><i className="fa fa-pie-chart"></i>交易统计<i className="fa fa-chevron-right arrow"></i></a></li>
                */}
            </ul>
        </div>
    </div>
)
