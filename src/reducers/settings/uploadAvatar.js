import {
    UPLOAD_AVATAR_SUCCESS,
    UPLOAD_AVATAR_FAILURE
} from "../../constants/actionTypes"
import { assign } from "../../utils"

export default (state, { type, payload }) => {
    switch(type) {
        case UPLOAD_AVATAR_SUCCESS:
            return assign({}, state, {
                message: payload.message,
                profile: assign({}, state.profile, {
                    avatar: payload.avatar
                })
            })
        case UPLOAD_AVATAR_FAILURE:
            return assign({}, state, {
                message: payload.message
            })
        default:
            return state
    }
}
