import { push } from "react-router-redux"
import { post } from "../../middleware/api"
import {
    SIGNUP_REQUEST,
    SIGNUP_SUCCESS,
    SIGNUP_FAILURE
} from "../../constants/actionTypes"

export default (data) => {
    return dispatch => {
        dispatch(signupRequest)

        return post("/users", data)
        .then(json => {
            dispatch(signupSuccess)
        }, error => {
            const messageMap = {
                // TODO
                "1004": "邮箱或密码格式不正确",
                "1007": "你的邮箱已经注册过"
            }

            dispatch(signupFailure({
                status: "warning",
                message: messageMap[error.code] || "哎呀，出现了一些错误，请再试一次"
            }))
        })
    }
}

const signupRequest = {
    type: SIGNUP_REQUEST
}

const signupSuccess = {
    type: SIGNUP_SUCCESS
}

function signupFailure(message) {
    return {
        type: SIGNUP_FAILURE,
        payload: {
            message
        }
    }
}
