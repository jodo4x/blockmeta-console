import { put } from "../../middleware/api"
import {
    UPDATE_PASSWORD_REQUEST,
    UPDATE_PASSWORD_SUCCESS,
    UPDATE_PASSWORD_FAILURE
} from "../../constants/actionTypes"

export default (data) => {
    return dispatch => {
        dispatch(updatePasswordRequest)

        put("/users/me/reset-password", data)
        .then(json =>
            dispatch(updatePasswordSuccess({
                status: "success",
                message: "你的密码已经更改成功"
            }))
        , error =>
            dispatch(updatePasswordFailure({
                status: "danger",
                message: "哎呀，出现了一些错误，请再试一次"
            }))
        )
    }
}

const updatePasswordRequest = {
    type: UPDATE_PASSWORD_REQUEST
}

function updatePasswordSuccess(message) {
    return {
        type: UPDATE_PASSWORD_SUCCESS,
        payload: {
            message
        }
    }
}

function updatePasswordFailure(message) {
    return {
        type: UPDATE_PASSWORD_FAILURE,
        payload: {
            message
        }
    }
}
