import { replace } from "react-router-redux"
import { cookie } from "../../utils"
import { get } from "../../middleware/api"
import BASE_URL from "../../constants/baseUrl"
import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    SHOW_CAPTCHA
} from "../../constants/actionTypes"

export default (data, rmb, captcha) => {
    return dispatch => {
        dispatch(loginRequest)

        return get(`/auth/token?code=${captcha||""}`, null, {
            headers: {
                Authorization: `Basic ${btoa(data.email + ":" + data.password)}`
            }
        })
        .then(json =>
            handleJson(dispatch, json, rmb)
        , error =>
            handleError(dispatch, error)
        )
    }
}

const loginRequest = {
    type: LOGIN_REQUEST
}

function loginSuccess() {
    return {
        type: LOGIN_SUCCESS,
        payload: { }
    }
}

function loginFailure(message) {
    return {
        type: LOGIN_FAILURE,
        payload: {
            message
        }
    }
}

function showCaptcha(captcha) {
    return {
        type: SHOW_CAPTCHA,
        payload: {
            captcha
        }
    }
}

function handleJson(dispatch, json, rmb = false) {
    const tokenMap = {
        "access_token": "_at_",
        "refresh_token": "_rt_"
    }

    let option = rmb ? { maxAge: 30*24*60*60 } : null

    for (let prop in tokenMap) {
        if (json[prop]) {
            cookie.set(tokenMap[prop], json[prop], option)
        }
    }

    dispatch(loginSuccess())
    dispatch(replace("/assets"))
}

function handleError(dispatch, error) {
    const messages = {
        "1010": "需要输入验证码，请再试一次",
        "1011": "验证码不正确，请再试一次",
        "1012": "邮箱或密码不正确",
        "1003": "邮箱或密码不正确",
        "1006": "你的邮箱还未激活"
    }

    if (error.code === 1010 || error.code === 1011) {
        getCaptcha(dispatch)
    }

    dispatch(loginFailure({
        status: "warning",
        message: messages[error.code] || "哎呀，出现了一些其它错误，请再试一次"
    }))
}

export function getCaptcha(dispatch) {
    get("/tools/captcha").then(json =>
        dispatch(showCaptcha(`${BASE_URL.replace(/\/api/, "") + json.url}`))
    , error => {
        // TODO
    })
}
