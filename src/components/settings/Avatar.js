import React from "react"

const Avatar = ({ profile, uploadAvatar }) => {
    let file

    return profile ? (
        <div className="text-center avatar-container">
            <div className="avatar">
                <img
                    src={profile.avatar ? ("//" + profile.avatar + "!/fw/240/fh/240") : "//maoxiang-static-head.b0.upaiyun.com/o4jnf5tz6z2vxl8g.jpg"}
                    alt={profile.nickname}
                    style={{ width: 110, height: 110 }}
                />
            </div>
            <div className="">
                <input
                    className="hidden"
                    type="file"
                    ref={node => file = node}
                    onChange={uploadAvatar}
                />
                <button
                    className="btn btn-link"
                    type="button"
                    onClick={() => {
                        file.click()
                    }}
                >
                    <span>更换头像</span>
                </button>
            </div>
        </div>
    ) : null
}

export default Avatar
