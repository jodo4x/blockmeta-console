var path = require("path")
var webpack = require("webpack")

var webpackCommonsChunkPlugin = new webpack.optimize.CommonsChunkPlugin("vendor", "vendor.js")
var vendor = [
    "babel-polyfill",
    "react",
    "react-dom",
    "react-router",
    "react-router-redux",
    "react-redux",
    "redux",
    "redux-thunk",
    "whatwg-fetch"
]

module.exports = {
    entry: {
        app: path.join(__dirname, "src"),
        vendor: process.env.NODE_ENV === "production" ? vendor : vendor.concat("redux-logger")
    },

    output: {
        path: path.join(__dirname, "build"),
        filename: "bundle.js"
    },

    module: {
        loaders: [
            { test: /\.js?$/, exclude: /node_modules/, loader: "babel?presets[]=es2015&presets[]=react" }
        ]
    },

    plugins: process.env.NODE_ENV === "production" ?
            [
                webpackCommonsChunkPlugin,
                new webpack.DefinePlugin({
                    "process.env": {
                        "NODE_ENV": JSON.stringify("production")
                    }
                }),
                new webpack.optimize.UglifyJsPlugin({ compress: { warnings: false } })
            ] :
            [
                webpackCommonsChunkPlugin
            ]
}
