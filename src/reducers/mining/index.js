import {
    GET_MINING_INFO_SUCCESS,
    GET_MINING_INFO_FAILURE
} from "../../constants/actionTypes"

import { assign } from "../../utils"

export default function mining(state, { type, payload }) {
    switch(type) {

        case "GET_MARKET":
            return assign({}, state, {
                market: payload.market
            })

        // 难度和算力
        case "GET_MINING_INFO_REQUEST":
            return assign({}, state, {
                spinners: assign({}, state.spinners, {
                    miningInfoSpinner: payload.isFetching
                })
            })
        case GET_MINING_INFO_SUCCESS:
            return assign({}, state, {
                miningInfo: payload.miningInfo,
                spinners: assign({}, state.spinners, {
                    miningInfoSpinner: payload.isFetching
                })
            })

        case GET_MINING_INFO_FAILURE:
            return assign({}, state, {
                message: payload.message,
                spinners: assign({}, state.spinners, {
                    miningInfoSpinner: payload.isFetching
                })
            })

        // 计算器
        case "CALCULATE_MINING_REQUEST":
            return assign({}, state, {
                errDetails: payload.errDetails
            })
        case "CALCULATE_MINING_FAILURE":
            return assign({}, state, {
                message: payload.message,
                errDetails: payload.errDetails
            })
        case "CALCULATE_MINING_SUCCESS":
            return assign({}, state, {
                calculateResult: payload.calculateResult
            })

        // 分布
        case "GET_DISTRIBUTION_REQUEST":
            return assign({}, state, {
                spinners: assign({}, state.spinners, {
                    distributionSpinner: payload.isFetching
                })
            })
        case "GET_DISTRIBUTION_SUCCESS":
            return Object.assign({}, state, {
                distribution: payload.distribution,
                spinners: assign({}, state.spinners, {
                    distributionSpinner: payload.isFetching
                })
            })
        case "GET_DISTRIBUTION_FAILURE":
            return assign({}, state, {
                message: payload.message,
                spinners: assign({}, state.spinners, {
                    distributionSpinner: payload.isFetching
                })
            })

        // 矿池
        case "GET_HASH_RATE_REQUEST":
            return assign({}, state, {
                spinners: assign({}, state.spinners, {
                    hashRateSpinner: payload.isFetching
                })
            })
        case "GET_HASH_RATE_SUCCESS":
            return Object.assign({}, state, {
                hashRate: payload.hashRate,
                spinners: Object.assign({}, state.spinners, {
                    hashRateSpinner: payload.isFetching
                })
            })
        case "GET_HASH_RATE_FAILURE":
            return Object.assign({}, state, {
                message: payload.message,
                spinners: Object.assign({}, state.spinners, {
                    hashRateSpinner: payload.isFetching
                })
            })

        default:
            return state
    }
}
