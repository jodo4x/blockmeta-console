import {
    GET_SMSCODE_SUCCESS,
    GET_SMSCODE_FAILURE
} from "../../constants/actionTypes"
import { assign } from "../../utils"

export default (state, { type, payload }) => {
    switch(type) {
        case GET_SMSCODE_SUCCESS:
        case GET_SMSCODE_FAILURE:
            return assign({}, state, {
                message: payload.message
            })
        default:
            return state
    }
}
