import {
    GET_RECOVERY_PASSWORD_EMAIL_SUCCESS,
    GET_RECOVERY_PASSWORD_EMAIL_FAILURE
} from "../../constants/actionTypes"
import { assign } from "../../utils"

export default (state, { type, payload }) => {
    switch(type) {
        case GET_RECOVERY_PASSWORD_EMAIL_SUCCESS:
            return assign({}, state, {
                isSubmitted: true
            })
        case GET_RECOVERY_PASSWORD_EMAIL_FAILURE:
            return assign({}, state, {
                message: payload.message
            })
        default:
            return state
    }
}
