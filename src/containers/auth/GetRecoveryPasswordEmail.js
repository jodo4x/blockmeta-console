import React, { PropTypes } from "react"
import { connect } from "react-redux"
import getRecoveryPasswordEmail from "../../actions/auth/getRecoveryPasswordEmail"
import Field from "../../components/form/Field"
import { Link } from "react-router"

export default connect(
    state => ({
        isSubmitted: state.isSubmitted
    })
)(({ isSubmitted = false, dispatch }) => {
    let emailRef

    return (
        <div className="panel panel-default">
            <div className="panel-body">
                {isSubmitted ? (
                    <div className="text-center" style={{ fontSize: 14, color: "#424f72", lineHeight: 1.65 }}>
                        <h2 className="text-capitalize text-center" style={{ color: "#1e294a", fontSize: 25, marginBottom: 20 }}>Blockmeta</h2>
                        <img src="//maoxiang-static-head.b0.upaiyun.com/tml151p081q8rq51.png" alt="" style={{ width: 70, marginBottom: 35 }} />
                        <p style={{ fontSize: 14 }}>找回密码的邮件已经发送至<span style={{ color: "" }}>你的邮箱</span>
                        <br />点击邮件中的链接进行下一步操作</p>
                        <hr />
                        <Link to="/login">返回登录页面</Link>
                    </div>
                ) : (
                    <form onSubmit={e => {
                        e.preventDefault()
                        dispatch(getRecoveryPasswordEmail({
                            email: emailRef.value.trim()
                        }))
                    }}>
                        <legend className="text-center text-capitalize">Blockmeta</legend>
                        <Field name="email" label="找回密码" _ref={node => emailRef = node} placeholder="请输入要找回密码的邮箱" />
                        <button className="btn btn-success btn-block btn-lg text-capitalize" type="submit">下一步</button>
                    </form>
                )}
            </div>
        </div>
    )
})
