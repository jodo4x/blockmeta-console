import {
    LOGOUT_SUCCESS,
    LOGOUT_FAILURE
} from "../../constants/actionTypes"
import { assign } from "../../utils"

export default (state, { type }) => {
    if (type === LOGOUT_SUCCESS) {
        return assign({}, state, {
            isAuthenticated: false
        })
    }

    return state
}
