import React, { PropTypes } from "react"
import { connect } from "react-redux"
import Overlay from "../components/Overlay"
import Message from "../components/Message"

const App = ({ isNavbarCollapsed, isFetching, message, children }) => (
    <div className="reactroot">
        <Message message={message} />
        <div className="container-fluid">
            <div className={isNavbarCollapsed ? "db-collapse" : "db-expand"}>
                {children}
            </div>
        </div>
        <Overlay isFetching={isFetching} />
    </div>
)

export default connect(
    state => ({
        isFetching: state.isFetching,
        isNavbarCollapsed: state.isNavbarCollapsed,
        message: state.message
    })
)(App)
