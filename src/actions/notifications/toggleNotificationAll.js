import {
    TOGGLE_NOTIFICATION_ALL
} from "../../constants/actionTypes"

export default (data) => (
    {
        type: TOGGLE_NOTIFICATION_ALL,
        payload: {
            data
        }
    }
)
