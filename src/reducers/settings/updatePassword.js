import {
    UPDATE_PASSWORD_SUCCESS,
    UPDATE_PASSWORD_FAILURE
} from "../../constants/actionTypes"
import { assign } from "../../utils"

export default (state, { type, payload }) => {
    switch(type) {
        case UPDATE_PASSWORD_SUCCESS:
        case UPDATE_PASSWORD_FAILURE:
            return assign({}, state, {
                message: payload.message
            })
        default:
            return state
    }
}
