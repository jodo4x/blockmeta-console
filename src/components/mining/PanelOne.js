import React from "react"
import echarts from "echarts"

import { connect } from "react-redux"
import getDistribution from "../../actions/mining/getDistribution"

const Loading = connect(
    state => ({
        isFetching: state.spinners.distributionSpinner
    })
)((props) => {
    return props.isFetching ? <span className="myLoading">正在加载...</span> : null
})

export default connect(
    state => ({
        distribution: state.distribution
    })
)(class PanelOne extends React.Component {
    constructor(props) {
        super(props)
        this.handleTabClick = this.handleTabClick.bind(this)
    }

    handleTabClick(e) {
        const parent = e.target.parentElement.parentElement // `<ul>`
        const siblings = parent.children // `<li>...`

        ;[].forEach.call(siblings, (el) => {
            el.classList.remove("active")
        })
        e.target.parentElement.classList.add("active")
    }

    componentDidMount() {
        this.props.dispatch(getDistribution({
            timespan: "1d"
        }))
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.distribution) {
            let options = {
                tooltip: {
                    trigger: "item",
                    formatter: "<strong>{a}</strong><br/>{b}: {c} <sup>({d}%)</sup>"
                },
                color: [
                    "#e6f3a4", "#a4e59b", "#68d4b2", "#54becc", "#6890d4", "#f0d487"
                ],
                series: [
                    {
                        name: "分布",
                        type: "pie",
                        radius: ["35%", "50%"],
                        data: (function(data) {
                            var ret = []
                            var i = 0
                            Object.keys(data||{}).forEach((el) => {
                                if (el !== "all") {
                                    ret[i] = {
                                        name: el,
                                        value: data[el].toFixed(2)
                                    }
                                    i++
                                }
                            })
                            return ret
                        }(nextProps.distribution))
                    }
                ]
            }

            let pieChart = echarts.init(document.getElementById("J__piechart"))
            pieChart.setOption(options)
        }
    }

    render() {
        let { dispatch } = this.props
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    <ul className="pull-right nav nav-pills p2-nav">
                        <li className="active">
                            <a href="javascript:;" onClick={(e) => {
                                this.handleTabClick(e)
                                dispatch(getDistribution({ timespan: "1d" }))
                            }}>24H</a>
                        </li>
                        <li>
                            <a href="javascript:;" onClick={(e) => {
                                this.handleTabClick(e)
                                dispatch(getDistribution({ timespan: "7d" }))
                            }}>7天</a>
                        </li>
                        <li>
                            <a href="javascript:;" onClick={(e) => {
                                this.handleTabClick(e)
                                dispatch(getDistribution({ timespan: "1m" }))
                            }}>30天</a>
                        </li>
                    </ul>
                    <h2 className="panel-title"><span>算力</span>分布</h2>
                </div>
                <div className="panel-body" style={{ position: "relative" }}>
                    <div id="J__piechart" style={{ width: "100%", height: 300 }}></div>
                    <Loading />
                </div>
            </div>
        )
    }
})
