import {
    QUERY_NOTIFICATIONS_SUCCESS,
    QUERY_NOTIFICATIONS_FAILURE,

    GET_NOTIFICATION_SUCCESS,
    GET_NOTIFICATION_FAILURE,

    DELETE_NOTIFICATION_SUCCESS,
    DELETE_NOTIFICATION_FAILURE,

    BATCH_DELETE_NOTIFICATIONS_SUCCESS,
    BATCH_DELETE_NOTIFICATIONS_FAILURE,

    BATCH_MARK_NOTIFICATIONS_SUCCESS,
    BATCH_MARK_NOTIFICATIONS_FAILURE,

    TOGGLE_NOTIFICATION,
    TOGGLE_NOTIFICATION_ALL,

    TOGGLE_NOTIFICATION_DISPLAY
} from "../../constants/actionTypes"
import { assign } from "../../utils"

export default (state, { type, payload }) => {
    let notifs, items

    switch(type) {
        case QUERY_NOTIFICATIONS_SUCCESS:
            // HACK: 给所有的记录手动添加`isChecked=false`状态
            payload.data.items.map(item => {
                return item.isChecked = false
            })
            return assign({}, state, {
                notifications: payload.data
            })

        case GET_NOTIFICATION_SUCCESS:
            notifs = state.notifications
            return assign({}, state, {
                notifications: assign({}, notifs, {
                    unread: notifs.unread - 1,
                    items: notifs.items.map(item => {
                        if (item.id === payload.id) {
                            item.is_read = true
                        }
                        return item
                    })
                })
            })

        case DELETE_NOTIFICATION_SUCCESS:
            notifs = state.notifications
            return assign({}, state, {
                notifications: assign({}, notifs, {
                    items: notifs.items.filter(item => item.id !== payload.id)
                })
            })

        case TOGGLE_NOTIFICATION:
            notifs = state.notifications
            notifs.items.map(item => {
                if (item.id === payload.id) {
                    item.isChecked = !item.isChecked
                }
                return item
            })
            return assign({}, state, {
                notifications: assign({}, notifs, {
                    batchItems: notifs.batchItems ?
                            notifs.batchItems.filter(id => id === payload.id).length ?
                            notifs.batchItems.filter(id => id !== payload.id) :
                            [ ...notifs.batchItems, payload.id ] :
                            [ payload.id ]
                })
            })

        case TOGGLE_NOTIFICATION_ALL:
            let items = payload.data.items
            items.map(item => {
                item.isChecked = payload.data.isAllSelected ? true : false
                return item
            })
            return assign({}, state, {
                notifications: assign({}, state.notifications, {
                    isAllSelected: payload.data.isAllSelected,
                    batchItems: payload.data.isAllSelected ? items.map(item => item.id) : []
                })
            })

        case TOGGLE_NOTIFICATION_DISPLAY:
            return assign({}, state, {
                notifications: assign({}, state.notifications, {
                    items: state.notifications.items.map(item => {
                        if (item.id === payload.id) {
                            item.isShown = !item.isShown
                        }
                        return item
                    })
                })
            })

        // TODO
        case BATCH_DELETE_NOTIFICATIONS_SUCCESS:
        case BATCH_MARK_NOTIFICATIONS_SUCCESS:

        case QUERY_NOTIFICATIONS_FAILURE:
        case GET_NOTIFICATION_FAILURE:
        case DELETE_NOTIFICATION_FAILURE:
        case BATCH_DELETE_NOTIFICATIONS_FAILURE:
        case BATCH_MARK_NOTIFICATIONS_FAILURE:
            return assign({}, state, {
                message: payload.message
            })
        default:
            return state
    }
}
