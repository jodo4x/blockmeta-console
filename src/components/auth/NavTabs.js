import React, { PropTypes } from "react"
import { Link } from "react-router"

export default ({ pathname, isFetching, handleClick }) => (
    <div className="">
        {pathname === "/reset-password" ? (
            <div style={{
                color: "#8f9fad",
                fontSize: 18,
                textAlign: "center",
                fontWeight: "bold",
                marginBottom: 35
            }}>找回密码</div>
        ) : (
            <ul className="nav nav-tabs text-capitalize">
                <li className={pathname === "/login" || pathname === "/recovery-password" ? "active" : ""}>
                    <Link
                        className={isFetching ? "disabled-link" : ""}
                        to="/login"
                        onClick={handleClick}
                    >
                        <span>登录</span>
                    </Link>
                </li>
                <li className={pathname === "/signup" ? "active" : ""}>
                    <Link
                        className={isFetching ? "disabled-link" : ""}
                        to="/signup"
                        onClick={handleClick}
                    >
                        <span>注册</span>
                    </Link>
                </li>
            </ul>
        )}
    </div>
)
