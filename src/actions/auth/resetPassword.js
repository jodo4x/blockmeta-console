import { put } from "../../middleware/api"
import { replace } from "react-router-redux"
import {
    RESET_PASSWORD_REQUEST,
    RESET_PASSWORD_SUCCESS,
    RESET_PASSWORD_FAILURE
} from "../../constants/actionTypes"

export default (data) => {
    return dispatch => {
        dispatch(resetPasswordRequest)

        put("/users/me/forget-password", data)
        .then(json => {
            dispatch(resetPasswordSuccess({
                status: "success",
                message: "你的密码已经重置"
            }))

            let timeoutId = setTimeout(() => {
                dispatch(replace("/login"))
                clearTimeout(timeoutId)
            }, 5000)
        }, error => {
            const messageMap = {
                "1004": "密码格式不正确",
                "1005": "对不起，该链接已经失效",
                "1013": "哎呀，出现了一些错误，我们无法找到该用户"
            }

            dispatch(resetPasswordFailure({
                status: "warning",
                message: messageMap[error.code]
            }))
        })
    }
}

const resetPasswordRequest = {
    type: RESET_PASSWORD_REQUEST
}

function resetPasswordSuccess(message) {
    return {
        type: RESET_PASSWORD_SUCCESS,
        payload: {
            message
        }
    }
}

function resetPasswordFailure(message) {
    return {
        type: RESET_PASSWORD_FAILURE,
        payload: {
            message
        }
    }
}
