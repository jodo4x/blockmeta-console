import React, { Component } from "react"
import { connect } from "react-redux"
import { bindActionCreators } from "redux"

import calculateMining from "../../actions/mining/calculate"

import PanelOne from "../../components/mining/PanelOne"
import PanelTwo from "../../components/mining/PanelTwo"
import PanelThree from "../../components/mining/PanelThree"
import PanelFour from "../../components/mining/PanelFour"
import PanelFive from "../../components/mining/PanelFive"

export default connect(
    state => ({
        calculateResult: state.calculateResult,
        errDetails: state.errDetails
    }),
    dispatch => bindActionCreators({
        calculateMining
    }, dispatch)
)(class Mining extends Component {
    render() {
        return (
            <div className="mining-container" style={{ paddingTop: 20 }}>
                <div className="clearfix">
                    <div className="pull-left" style={{ width: "30%", paddingRight: 20 }}>
                        <PanelOne />
                    </div>

                    <div className="pull-right" style={{ width: "70%" }}>
                        <PanelTwo />
                    </div>
                </div>

                <div className="clearfix">
                    <div className="pull-left" style={{ width: "60%", paddingRight: 20 }}>
                        <PanelThree {...this.props} />
                    </div>

                    <div className="pull-right" style={{ width: "40%" }}>
                        <PanelFour {...this.props} />
                    </div>
                </div>

                <div className="">
                    <PanelFive />
                </div>
            </div>
        )
    }
})
