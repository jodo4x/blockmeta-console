import React from "react"

const Captcha = ({ _ref, src, handleGetCaptcha }) => {
    return src ? (
        <div className="form-group">
            <label className="control-label sr-only" htmlFor="field_captcha">captcha</label>
            <div className="row">
                <div className="col-xs-6">
                    <input
                        className="form-control input-lg"
                        id="field__captcha"
                        type="text"
                        ref={_ref}
                        placeholder="输入验证码"
                    />
                </div>
                <div className="col-xs-6">
                    <img src={src} alt="captcha" onClick={handleGetCaptcha} style={{ cursor: "pointer" }} />
                </div>
            </div>
        </div>
    ) : null
}

export default Captcha
