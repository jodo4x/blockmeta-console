import {
    TOGGLE_NOTIFICATION,
    TOGGLE_NOTIFICATION_ALL
} from "../../constants/actionTypes"

export default (id) => (
    {
        type: TOGGLE_NOTIFICATION,
        payload: {
            id: id
        }
    }
)
