import { post } from "../../middleware/api"
import {
    GET_SMSCODE_REQUEST,
    GET_SMSCODE_SUCCESS,
    GET_SMSCODE_FAILURE
} from "../../constants/actionTypes"

export default (data) => {
    return dispatch => {
        dispatch(getSMSCodeRequest)

        post("/users/me/reset-mobile", data)
        .then(json =>
            dispatch(getSMSCodeSuccess({
                status: "success",
                message: `验证码已经发送到手机${data.mobile}`
            }))
        , error => {
            const messageMap = {
                "1004": "手机号码不正确",
                "1007": "对不起，该手机已经被绑定"
            }

            dispatch(getSMSCodeFailure({
                status: "danger",
                message: messageMap[error.code] || "哎呀，出现了一些错误，请再试一次"
            }))
        })
    }
}

const getSMSCodeRequest = {
    type: GET_SMSCODE_REQUEST
}

function getSMSCodeSuccess(message) {
    return {
        type: GET_SMSCODE_SUCCESS,
        payload: {
            message
        }
    }
}

function getSMSCodeFailure(message) {
    return {
        type: GET_SMSCODE_FAILURE,
        payload: {
            message
        }
    }
}
