import { TOGGLE_NAVBAR } from "../constants/actionTypes"

const toggleNavbar = {
    type: TOGGLE_NAVBAR,
    payload: {
    }
}

export default toggleNavbar
