import React from "react"
import { connect } from "react-redux"
import { Link } from "react-router"
import login from "../../actions/auth/login"
import Field from "../../components/form/Field"
import Captcha from "../../components/auth/Captcha"
import { getCaptcha } from "../../actions/auth/login"

import { EMAIL_PATTERN, PASSWORD_PATTERN } from "../../constants/pattern"

const Login = ({ captcha, dispatch }) => {
    let emailRef, passwordRef, captchaRef, rmb, n1, n2

    return (
        <div className="panel panel-default">
            <div className="panel-body">
                <form onSubmit={e => {
                    e.preventDefault()

                    let captchaValue
                    if (captcha) {
                        captchaValue = captchaRef.value.trim()
                    }
                    dispatch(login({
                        email: emailRef.value.trim(),
                        password: passwordRef.value.trim()
                    }, rmb.checked, captchaValue))
                }}>
                    <legend className="text-center text-capitalize">Blockmeta</legend>

                    <Field
                        name="email"
                        label="邮箱"
                        placeholder="请输入你的邮箱"
                        _ref={node => emailRef = node}
                        onBlur={(e) => {
                            if (!EMAIL_PATTERN.test(e.target.value.trim())) {
                                emailRef.classList.add("error")
                                n1.classList.remove("hide")
                            } else {
                                emailRef.classList.remove("error")
                                n1.classList.add("hide")
                            }
                        }}
                    >
                        <p className="text-danger hide" style={{ marginTop: 5 }} ref={node => n1 = node}>邮箱格式不对</p>
                    </Field>

                    <Field
                        name="password"
                        label="密码"
                        type="password"
                        placeholder="请输入登录密码"
                        _ref={node => passwordRef = node}
                        onBlur={(e) => {
                            if (!PASSWORD_PATTERN.test(e.target.value.trim())) {
                                passwordRef.classList.add("error")
                                n2.classList.remove("hide")
                            } else {
                                passwordRef.classList.remove("error")
                                n2.classList.add("hide")
                            }
                        }}
                    >
                        <p className="text-danger hide" style={{ marginTop: 5 }} ref={node => n2 = node}>密码格式不对</p>
                    </Field>

                    <Captcha
                        src={captcha}
                        _ref={node => captchaRef = node}
                        handleGetCaptcha={() => {
                            getCaptcha(dispatch)
                        }}
                    />

                    <div className="clearfix">
                        <div className="checkbox pull-left">
                            <label>
                                <input type="checkbox" ref={node => rmb = node} />
                                <span>保持登录</span>
                            </label>
                        </div>
                        <div className="pull-right">
                            <Link to="/recovery-password" className="text-muted">忘记密码?</Link>
                        </div>
                    </div>
                    <button className="btn btn-success btn-lg btn-block" type="submit">登录</button>
                </form>
            </div>
        </div>
    )
}

export default connect(
    state => ({
        captcha: state.captcha
    })
)(Login)
