import { destroy } from "../../middleware/api"
import {
    DELETE_ASSET_REQUEST,
    DELETE_ASSET_SUCCESS,
    DELETE_ASSET_FAILURE
} from "../../constants/actionTypes"
import loadMyAssets from "./loadMyAssets"

export default (id) =>
    dispatch => {
        dispatch(deleteAssetRequest)
        destroy(`/assets/${id}`)
        .then(json => {
            dispatch(deleteAssetSuccess(id))
            dispatch(loadMyAssets())
        }, err => {
            dispatch(deleteAssetFailure({
                status: "warning",
                message: "删除失败，请再试一次"
            }))
        })
    }

const deleteAssetRequest = {
    type: DELETE_ASSET_REQUEST
}

function deleteAssetSuccess(id) {
    return {
        type: DELETE_ASSET_SUCCESS,
        payload: {
            id,
            isAssetDeleted:  true
        }
    }
}

function deleteAssetFailure(message) {
    return {
        type: DELETE_ASSET_FAILURE,
        payload: {
            message
        }
    }
}
