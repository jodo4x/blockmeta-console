import React from "react"

const Footer = () => (
    <div className="text-center footer">
        <div>&copy; Blockmeta <br /><a href="https://blockmeta.com">返回首页</a></div>
    </div>
)

export default Footer
