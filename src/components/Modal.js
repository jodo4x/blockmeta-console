import React, { Component } from "react"

export default class Modal extends Component {
    constructor(props) {
        super(props)
        this.open = this.open.bind(this)
        this.handleDismiss = this.handleDismiss.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.state = {
            isOpen: false
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps !== this.props) {
            if (nextProps.isMonitorUpdated) {
                this.handleDismiss()
            }

            if (nextProps.isAssetDeleted) {
                this.handleDismiss()
            }
        }
    }

    handleDismiss() {
        this.setState({
            isOpen: false
        })
    }

    handleSubmit() {
        this.props.handleSubmit()
    }

    open() {
        this.setState({
            isOpen: true
        })
    }

    render() {
        let { title, children } = this.props

        return this.state.isOpen ? (
            <div className="modal in" style={{ display: "block" }}>
                <div className="modal-backdrop"></div>
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button className="close" type="button" onClick={this.handleDismiss}><span>&times;</span></button>
                            <h4 className="modal-title">{title}</h4>
                        </div>
                        <div className="modal-body">
                            {children}
                        </div>
                        <div className="modal-footer">
                            <button className="btn btn-default" type="button" onClick={this.handleDismiss}>取消</button>
                            <button className="btn btn-success" type="button" onClick={this.handleSubmit}>确定</button>
                        </div>
                    </div>
                </div>
            </div>
        ) : null
    }
}
