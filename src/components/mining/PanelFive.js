import React from "react"
import Select from "react-select"
import echarts from "echarts"
import { push } from "react-router-redux"

import { connect } from "react-redux"
import getHashRate from "../../actions/mining/getHashRate"

const Loading = connect(
    state => ({
        isFetching: state.spinners.hashRateSpinner
    })
)((props) => {
    return props.isFetching ? <span className="myLoading">正在加载...</span> : null
})

export default connect(
    state => ({
        hashRate: state.hashRate
    })
)(class PanelFive extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            selected: "mining"
        }
        this.handleTabClick = this.handleTabClick.bind(this)
    }

    handleTabClick(e) {
        const parent = e.target.parentElement.parentElement // `<ul>`
        const siblings = parent.children // `<li>...`

        ;[].forEach.call(siblings, (el) => {
            el.classList.remove("active")
        })
        e.target.parentElement.classList.add("active")
    }

    componentDidMount() {
        let { dispatch } = this.props
        dispatch(getHashRate({
            timespan: "1d"
        }))
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps !== this.props && nextProps.hashRate) {
            const hashRate = nextProps.hashRate

            const legends = [], timeline = [], series = []
            let count = 0
            for (let p in hashRate) {
                legends.push(p)

                const data = []
                let len = 23
                hashRate[p].forEach((item) => {
                    const d = new Date(item[1]*1000)
                    const curr_hr = this.refs.curr_hr.getAttribute("data-value")
                    data.push(item[0])
                    // timeline.push(`${d.getFullYear()}/${d.getMonth()+1}/${d.getDate()} ${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}}`)
                    if (count === 0) {
                        // timeline.push(`${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}`)
                        if (curr_hr === "1d") {
                            // timeline.push(`${d.getHours()}点`)
                            timeline.push(`${d.getHours()}:00`)
                        }
                        if (curr_hr === "7d") {
                            // const map = [ "天", "一", "二", "三", "四", "五", "六" ]
                            // timeline.push(`星期${map[d.getDay()]}`)
                            timeline.push(`${d.getMonth()+1}/${d.getDate()}`)
                        }
                        if (curr_hr === "30d") {
                            timeline.push(`${d.getMonth()+1}/${d.getDate()}`)
                        }
                    }
                })
                // timeline.reverse()
                series.push({
                    name: p,
                    type: "line",
                    data: data
                })
                count++
            }

            // OPTIONS
            // ============================================
            const grid = {
                left         : 50,
                bottom       : 30,
                x            : 20,
                x2           : 20,
                y            : 30,
                y2           : 20,
                containLabel : false
            }

            const xAxis1 = {
                // type: "category",
                splitLine: false,
                boundaryGap: false,
                // data: ["星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"],
                data: timeline,
                // splitNumber: 7,
                axisLine: { lineStyle: { color: "#f2f4f7" } },
                axisTick: { color: "#5d6577", lineStyle: { color: "#f2f4f7" }, interval: 0, length: 10 }
            }

            const yAxis1 = {
                type: "value",
                splitLine: { lineStyle: { color: "#f2f4f7" } },
                axisLine: { lineStyle: { color: "transparent" } },
                axisTick: { color: "#5d6577", lineStyle: { color: "transparent" } }
            }
            // ============================================

            echarts.init(document.getElementById("J__barchart"))
            .setOption({
                legend: {
                    data: legends,
                    selectedMode: "multiple"
                },
                grid: grid,
                xAxis: [ xAxis1 ],
                yAxis: [ yAxis1 ],
                color: [ "#e6f3a4", "#39ca74", "#68d4b2", "#54becc", "#688fd4", "#f0d487" ],
                series: series,
                tooltip: {
                    trigger: "axis",
                    borderRadius: 1,
                    backgroundColor: "#fff",
                    formatter: (p) => {
                        var rslt = `
                        <div class="axispointer">
                            <div class="datetime">${p[0].name}</div>
                            <div style="font-size: 12px;">
                                <div><span class="d1 en">${p[0].seriesName}:</span>${p[0].value}<sup>(PH/s)</sup></div>
                                <div><span class="d2 en">${p[1].seriesName}:</span>${p[1].value}<sup>(PH/s)</sup></div>
                                <div><span class="d3 en">${p[2].seriesName}:</span>${p[2].value}<sup>(PH/s)</sup></div>
                                <div><span class="d4 en">${p[3].seriesName}:</span>${p[3].value}<sup>(PH/s)</sup></div>
                                <div><span class="d5 en">${p[4].seriesName}:</span>${p[4].value}<sup>(PH/s)</sup></div>
                            </div>
                        </div>
                        `
                        return rslt
                    },
                    textStyle: { color: "#1e294a" },
                    axisPointer: { type: "line", lineStyle: { color: "#e9e9e9", width: 1 } }
                }
            })
        }
    }

    render() {
        let { dispatch } = this.props
        return (
            <div className="panel panel-default">
                <div className="panel-heading">
                    <div className="pull-right">
                        <div className="pull-left" style={{ position: "relative", marginRight: 20, top: -3 }}>
                            <Select
                                options={
                                    [{label: "算力", value: "mining"}, {label: "收益", value: "lucky"}].map((item, index) => {
                                        return {
                                            label: item.label,
                                            value: item.value
                                        }
                                    })
                                }
                                value={this.state.selected}
                                clearable={false}
                                searchable={false}
                                onChange={(selected) => this.setState({ selected: selected.value })}
                            />
                        </div>
                        <ul className="pull-left nav nav-pills p2-nav">
                            <li className="active" ref="curr_hr" data-value="1d">
                                <a href="javascript:;" onClick={(e) => {
                                    this.handleTabClick(e)
                                    this.refs.curr_hr.setAttribute("data-value", "1d")
                                    dispatch(getHashRate({ timespan: "1d" }))
                                }}>24H</a>
                            </li>
                            <li>
                                <a href="javascript:;" onClick={(e) => {
                                    this.handleTabClick(e)
                                    this.refs.curr_hr.setAttribute("data-value", "7d")
                                    dispatch(getHashRate({ timespan: "7d" }))
                                }}>7天</a>
                            </li>
                            <li>
                                <a href="javascript:;" onClick={(e) => {
                                    this.handleTabClick(e)
                                    this.refs.curr_hr.setAttribute("data-value", "30d")
                                    dispatch(getHashRate({ timespan: "30d" }))
                                }}>30天</a>
                            </li>
                        </ul>
                    </div>
                    <h2 className="panel-title"><span>各矿池</span></h2>
                </div>

                <div className="panel-body">
                    <div id="J__barchart" style={{ width: "100%", height: 350 }}></div>
                    <Loading />
                </div>
            </div>
        )
    }
})
