import {
    GET_MODAL_ASSET
} from "../../constants/actionTypes"

export default (id) => {
    return {
        type: GET_MODAL_ASSET,
        payload: {
            id
        }
    }
}
