import React from "react"
import NewAssetRow from "./NewAssetRow"
import Asset from "./Asset"

export default (props) =>
    <table className="table">
        <thead id="J__thead">
            <tr>
                <th style={{width: "35%"}}>地址</th>
                <th style={{width: "20%"}}>地址类型</th>
                <th style={{width: "20%"}}>标识</th>
                <th style={{width: "25%"}} className="text-right">操作</th>
            </tr>
        </thead>
        <tbody>
            {props.isAddNewAsset ? <NewAssetRow {...props} ref={props.type} /> : null}

            {(props.isAddNewAsset || props.assets.items.length) ?
                props.assets.items.map((item, index) => {
                    return (
                        <Asset
                            key={index}
                            asset={item}
                            {...props}
                        />
                    )
                }) :
                (
                    <tr>
                        <td className="text-center" colSpan="4">
                            <div className="well">还未添加任何资源</div>
                        </td>
                    </tr>
                )
            }
        </tbody>
    </table>
