import React, { PropTypes, Component } from "react"
import { connect } from "react-redux"
import { assign } from "../../utils"
import clickTab from "../../actions/auth/clickTab"
import NavTabs from "../../components/auth/NavTabs"
import Footer from "../../components/auth/Footer"

class Auth extends Component {
    componentWillMount() {
        if (this.props.isAuthenticated) {
            this.context.router.replace("/foo")
        }
    }

    render() {
        let { location, isFetching, handleClick, children } = this.props
        return (
            <div className="auth-container">
                <NavTabs
                    isFetching={isFetching}
                    pathname={location.pathname}
                    handleClick={handleClick}
                />
                {children}
                <Footer />
            </div>
        )
    }
}

Auth.propTypes = {
    isFetching: PropTypes.bool.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
    handleClick: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired
}

Auth.contextTypes = {
    router: PropTypes.object.isRequired
}

export default connect(
    state => ({
        isFetching: state.isFetching,
        isAuthenticated: state.isAuthenticated
    }),

    null,

    (stateProps, dispatchProps, ownProps) => {
        let { isFetching, isAuthenticated } = stateProps
        let { dispatch } = dispatchProps

        return assign({}, ownProps, {
            isFetching: isFetching,
            isAuthenticated: isAuthenticated,
            handleClick: () => {
                if (isFetching) return
                dispatch(clickTab)
            }
        })
    }
)(Auth)
