import React, { PropTypes } from "react"

const Field = ({
    _ref,
    name,
    label,
    value,
    children,
    type = "text",
    placeholder = "",
    onBlur = null,
    onChange= null
}) => (
    <div className="form-group">
        <label className="control-label" htmlFor={"field__" + name}>{label}</label>
        {type === "static" ?
            <p className="form-control-static">{value}</p> :
            <input
                className="form-control input-lg"
                id={"field__" + name}
                type={type}
                ref={_ref}
                value={value}
                placeholder={placeholder}
                onBlur={onBlur}
                onChange={onChange}
            />
        }
        {children}
    </div>
)

Field.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    type: PropTypes.string,
    value: PropTypes.any,
    placeholder: PropTypes.string,
    children: PropTypes.node
}

export default Field
