import { patch } from "../../middleware/api"
import {
    ADD_ASSET_MONITOR_REQUEST,
    ADD_ASSET_MONITOR_SUCCESS,
    ADD_ASSET_MONITOR_FAILURE
} from "../../constants/actionTypes"
import loadMyAssets from "./loadMyAssets"

export default (id, data) =>
    dispatch => {
        dispatch(addAssetMonitorRequest)

        patch(`/assets/${id}/monitor`, data)
        .then(json => {
            dispatch(addAssetMonitorSuccess({
                status: "success",
                message: "添加监控成功"
            }))
            dispatch(loadMyAssets())
        }, err => {
            const messageMap = {
                "1004": "参数不全或不合法"
            }

            return dispatch(addAssetMonitorFailure({
                status: "warning",
                message: messageMap[err.code] || "哎呀，出现了一些未知错误，请再试一次"
            }))
        })
    }

const addAssetMonitorRequest = {
    type: ADD_ASSET_MONITOR_REQUEST
}

function addAssetMonitorSuccess(message) {
    return {
        type: ADD_ASSET_MONITOR_SUCCESS,
        payload: {
            message,
            isMonitorUpdated: true
        }
    }
}

function addAssetMonitorFailure(message) {
    return {
        type: ADD_ASSET_MONITOR_FAILURE,
        payload: {
            message,
            isMonitorUpdated: false
        }
    }
}
