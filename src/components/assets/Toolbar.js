import React from "react"

export default (props) =>
    <div className="navbar">
        <button
            className="btn btn-success navbar-btn"
            type="button"
            onClick={props.handleAddAsset}
        >
            <i className="fa fa-plus-circle"></i>
            <span>添加地址</span>
        </button>
        <form className="navbar-form navbar-right">
            <div className="form-group">
                <input
                    className="form-control"
                    style={{
                        backgroundImage: "url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAADhAJiYAAAABGdBTUEAALGPC/xhBQAABQhJREFUWAnFV31MHFUQf/P2OGiBKwWhFsV61BJtrJ9t/KpN0zbRqEmbCpd+aEg5qEkrJBUoyj9eTGpioXCh1IRAi2DSmoP2D00jKDFoUhNTjaWtJMZErfiVNhaklY/e7RtnFo8cyO0ua4WXHLvvzfzm/WZ23psBxOwG1DYdzwgrSNEkaAkiPPL9+ODVw2Vl47MzE18b4osmJLubmhLy3IvXS6VtAYlrEcELIFJZigJvgIBf6fWs0MUHo1Lvqi7yXZlAOvtrSqi2tWOrJmQ1mX5YIIYR4Gui0UckfkEUOhHLoPV7BMBq0slAIS7TejCCQ41Vfv81J5RmJPRmY1tG0sLkIAC8QAR+QgUNutJPVZb4fphpk7qWUDpIsUGALBUC1hGpPgXhkopd287OpG+29i9Ch46dyJHCfVKAWIOI9WMjfx2ofrnwDzMjUVkgEJCeO+59kRwJEikNlL59X7HvdFRu5zmFEHsqpOwmLx9AVP5yf0G7HSPTdWqaO+/TNMFOLVU6PFNR/Pxn03XizWWMAFDTGsi71f+FDNurLMk/H0b1HDk2TMTaDh4J3Rqzj+nrJKHals7NNNlJ2kGnkYndqcrv+1boehGt3elaKN+IlZm9G4RKGxoSyZPXOYHHRsdsg80Ms+yVYl8X3Q3tAmFXXWtolZU+yw1CuSm3PWnkDZ2m1/bsHLQDtKsTUeEaAASB0m8HYxCiyGwlT8ZvRPQOO6DZ6FQWb7tI+mfotyUQaE2ywsqCgpBG0aEI4blXX/INWAGcyOlG76bDssyTnbrcCi8f2RRZjIBeumX7SJkeN3+AjmxboKasCaF0pVIpSCYPuCb9L0PX1GU2DELLtNpAolJGHnFtslJ2KgfdFWEsgnJZ2ZDcQnDVJvV0K2Xn8qhtOWRlQ3I/Q5nzG9WQlVbKTuWUDisYqwT+aGVDGs0ViC8pm9cE6lvTrABO5CBhA+GGNKW+s8JP5I/C98mL9FRP6kYrwGzlB5qPL6F0eIpytIdu7qtWeIPQ+OjIaWq0rlBPs2/iXrKC2ZcnaYlFdL48CtUxOyiDEPc75EEdHf8nHn0auMDelFHTHPJKEJV0aM5cH/jmYztGDUKsODymGim056WUQbuF0GyDurrQAs0lm4lMso6inJo34+ibYVg2SSiw13ddjygKL2pUCE/VNnXebQWOJ2cyIk22UMQ5J/dXFuV/EU93+vokIRZUlPi+UkJsJ0PZ0g2f1LeEnp0OsJofbO1cQWT4kOwwdFF4ubW1wkXlU1rY6GJty8l1UsN3iJiX+uq2SEQF9+/2nYvKZ3q+dfREtgvchdRnVNF/IylKYQWR8tKvjO6fI9cuXSwjYuSv+ZiREEPqm95digkLAmSwhKZASf+pAPURvV5QIvK7ZpSahHQqB3kg5Ho+2nyaWA+Fqi4vKvjcaPpzVh2m07vHLqm4hIiEMQ4dfe9+AFchTTYTudx/lqc8KIp8v/SAUu1//tz/YWwkZkvKklB0Z26ujH4mQeQKBVlcKKkRHNRBXOIb2OzSiyWFSrw9PHChNJZ0dA9+2iYUC3LybpeU5sS4E0xvby8+9tDKLnda1i0SYG+iZ0kmz3k91t6cEeJNefPHHzQnNaeEYklxhPj0TY/UnBOKkuLPFSXFn3GRO7+7v7+DysQ8Df58nsT87tuXSyOncu6CzLyNa3vmjRDHgSPCkUpalJVFmb0jzZ3UPk/xmbotXwlUrpbRKvwNhyv9hm5t0XQAAAAASUVORK5CYII=)",
                        backgroundSize: "14px 14px",
                        backgroundRepeat: "no-repeat",
                        backgroundPosition: "3px center",
                        paddingLeft: 20
                    }}
                    id="field__keyword"
                    placeholder="搜索"
                    onBlur={(e) => props.handleFilterAssets({
                        q: e.target.value.trim()
                    })}
                />
            </div>
        </form>
    </div>
