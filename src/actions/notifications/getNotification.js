import { get } from "../../middleware/api"
import {
    GET_NOTIFICATION_REQUEST,
    GET_NOTIFICATION_SUCCESS,
    GET_NOTIFICATION_FAILURE
} from "../../constants/actionTypes"
import toggleNotificationDisplay from "./toggleNotificationDisplay"
import queryNotifications from "./queryNotifications"

export default (id) => {
    return dispatch => {
        dispatch(getNotificationRequest)

        get(`/messages/${id}`)
        .then(json => {
            dispatch(getNotificationSuccess(id))
            dispatch(toggleNotificationDisplay(id))
            // dispatch(queryNotifications())
        }, error =>
            dispatch(getNotificationFailure({
                status: "warning",
                message: "哎呀，遇到一些错误，请再试一次"
            }))
        )
    }
}

const getNotificationRequest = {
    type: GET_NOTIFICATION_REQUEST
}

function getNotificationSuccess(id) {
    return {
        type: GET_NOTIFICATION_SUCCESS,
        payload: {
            id
        }
    }
}

function getNotificationFailure(message) {
    return {
        type: GET_NOTIFICATION_FAILURE,
        payload: {
            message
        }
    }
}
