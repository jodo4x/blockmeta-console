import {
    CLICK_ADD_ASSET,
    LOAD_MY_ASSETS_REQUEST,
    LOAD_MY_ASSETS_SUCCESS,
    LOAD_MY_ASSETS_FAILURE,

    CREATE_ASSET_SUCCESS,
    CREATE_ASSET_FAILURE,

    DELETE_ASSET_SUCCESS,
    DELETE_ASSET_FAILURE,

    ADD_ASSET_MONITOR_REQUEST,
    ADD_ASSET_MONITOR_SUCCESS,
    ADD_ASSET_MONITOR_FAILURE,

    GET_MODAL_ASSET,

    QUERY_ASSETS_SUCCESS,
    QUERY_ASSETS_FAILURE,

    GET_ASSET_SUCCESS,
    GET_ASSET_FAILURE
} from "../../constants/actionTypes"
import { assign } from "../../utils"

export default (state, { type, payload }) => {
    switch(type) {
        case CLICK_ADD_ASSET:
            return assign({}, state, {
                isAddNewAsset: payload.isShown
            })
        case LOAD_MY_ASSETS_SUCCESS:
            return assign({}, state, {
                statistics: payload.statistics,
                assets: payload.assets
            })

        case "GET_ASSETS_STATE_FAILURE":
        case LOAD_MY_ASSETS_FAILURE:
            return assign({}, state, {
                message: payload.message
            })

        case CREATE_ASSET_SUCCESS:
            return assign({}, state, {
                message: payload.message,
                isAddNewAsset: false,
                assets: assign({}, state.assets||{}, {
                    items: [
                        payload.asset,
                        ...(state.assets.items||[])
                    ]
                })
            })
        case DELETE_ASSET_FAILURE:
        case CREATE_ASSET_FAILURE:
        case QUERY_ASSETS_FAILURE:
        case GET_ASSET_FAILURE:
            return assign({}, state, {
                message: payload.message
            })
        case DELETE_ASSET_SUCCESS:
            return assign({}, state, {
                isAssetDeleted: payload.isAssetDeleted,
                assets: assign({}, state.assets, {
                    items: state.assets.items.filter(item => item.id !== payload.id)
                })
            })
        case ADD_ASSET_MONITOR_REQUEST:
            return assign({}, state, {
                isMonitorUpdated: false
            })
        case ADD_ASSET_MONITOR_SUCCESS:
        case ADD_ASSET_MONITOR_FAILURE:
            return assign({}, state, {
                isMonitorUpdated: payload.isMonitorUpdated,
                message: payload.message
            })

        case GET_MODAL_ASSET:
            return assign({}, state, {
                isMonitorUpdated: false,
                modalAsset: state.assets.items.filter(item => item.id === payload.id)[0]
            })

        case QUERY_ASSETS_SUCCESS:
            return assign({}, state, {
                assets: payload.assets
            })
        case GET_ASSET_SUCCESS:
            return assign({}, state, {
                modalAsset: assign({}, state.modalAsset, payload.asset)
            })

        case "GET_ASSETS_STATE_SUCCESS":
            return assign({}, state, {
                statistics: payload.statistics
            })

        default:
            return state
    }
}
