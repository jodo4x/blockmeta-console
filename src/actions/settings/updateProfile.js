import { patch } from "../../middleware/api"
import {
    UPDATE_PROFILE_REQUEST,
    UPDATE_PROFILE_SUCCESS,
    UPDATE_PROFILE_FAILURE
} from "../../constants/actionTypes"

export default (data) => {
    return dispatch => {
        dispatch(updateProfileRequest)

        patch("/users/me", data)
        .then(json =>
            dispatch(updateProfileSuccess({
                message: {
                    status: "success",
                    message: "你的个人资料已经更改成功"
                },
                data: json
            }))
        , error =>
            dispatch(updateProfileFailure({
                status: "danger",
                message: "哎呀，出现了一些错误，请再试一次"
            }))
        )
    }
}

const updateProfileRequest = {
    type: UPDATE_PROFILE_REQUEST
}

function updateProfileSuccess({ message, data }) {
    return {
        type: UPDATE_PROFILE_SUCCESS,
        payload: {
            message,
            data
        }
    }
}

function updateProfileFailure(message) {
    return {
        type: UPDATE_PROFILE_FAILURE,
        payload: {
            message
        }
    }
}
